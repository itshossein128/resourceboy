<?php include 'header2.php'; ?>
    <div class="search-page__search-field">
        <div class="container-fluid d-flex flex-column align-items-center">
            <div class="search-page__input-container">
                <label class="search-page__search-input">
                    <img data-src="assets/images/dark-search-icon.svg" class="js-lazy">
                    <input type="search" placeholder="Search">
                    <button class="clear-search-input d-none"><img data-src="assets/images/close-icon.svg" class="js-lazy">
                    </button>
                </label>
            </div>
            <div class="search-page__desc">
                <span>1,183</span><span> Results for</span><span class="searched-text"> "design"</span><span> in Graphics</span>
            </div>
            <div class="search-page__categories">
                <button class="active" data-open="graphics">
                    Graphics <span>1243</span>
                </button>
                <button data-open="fonts">
                    Fonts <span>234</span>
                </button>
                <button data-open="brand-fonts">
                    Brand Fonts <span>124</span>
                </button>
                <button data-open="blog">
                    Blog <span>12</span>
                </button>
            </div>
        </div>
    </div>
    <div class="container-fluid main-content-container">
        <main class="">
            <section class="articles index__articles">
                <div data-target="graphics">
                    <div class="d-flex flex-column">
                        <span class="font-14 mb-10">License</span>
                        <div class="d-flex align-items-center">
                            <label class="commercial-checkbox-container ">
                                <input type="checkbox">
                                <span>Only commercial use</span>
                            </label>
                        </div>
                    </div>
                    <div class="articles-container articles-container-7 masonry-container">
                        <?php include 'articles-loading.php' ?>
                        <div class="gutter-sizer"></div>
                        <article class="masonry-item pin">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png"
                                         data-img-width="241" data-img-height="355" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cum ex
                                        facere laborum minus molestiae omnis quia reiciendis rem rerum.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item pin">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/img-bg-white.jpg"
                                         data-img-width="600" data-img-height="400" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque dicta harum id
                                        iste nesciunt perspiciatis quae repellendus ullam ut voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png"
                                         data-img-width="241" data-img-height="309" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita laborum nihil
                                        qui quis, reiciendis sapiente tempora. Aliquam dicta earum sit.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, animi
                                        beatae dolores illo in molestiae quaerat velit voluptatum. A, laudantium?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus architecto
                                        autem blanditiis commodi consequatur illum iste optio sit temporibus tenetur.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ducimus ipsa
                                        laboriosam laborum neque possimus quasi saepe vel voluptas voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, doloribus
                                        error facere itaque molestias quasi qui quia reiciendis temporibus ut?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, distinctio dolore
                                        eveniet itaque labore nesciunt perspiciatis. Iusto natus nobis quasi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto
                                        aspernatur beatae ea eaque neque non officiis praesentium repudiandae voluptate!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi dolorem,
                                        doloremque facilis modi natus neque quia quis quo repellat!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti
                                        dolorem ea esse fuga magnam maiores modi, nam provident quibusdam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores enim
                                        est et exercitationem fugiat nam neque obcaecati saepe totam!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png"
                                         data-img-width="241" data-img-height="309" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi est
                                        expedita fugiat ratione recusandae repellendus sed voluptates. Omnis, optio!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png"
                                         data-img-width="241" data-img-height="309" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quae, quis?
                                        Debitis, dignissimos dolor eligendi maxime mollitia nostrum quaerat qui.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, quaerat, ullam?
                                        Architecto dolore ipsam molestias neque nulla quibusdam quod sunt!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquid consectetur
                                        consequatur eos nostrum provident reprehenderit, repudiandae sint ullam
                                        voluptate.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam doloribus
                                        dolorum explicabo libero molestiae mollitia porro ratione repudiandae saepe?
                                        Magnam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png"
                                         data-img-width="241" data-img-height="309" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid
                                        aspernatur earum perferendis perspiciatis. Facilis hic ipsum iste officiis
                                        ratione!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi incidunt
                                        ipsum molestias nihil quae quam quia, soluta sunt voluptatum.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti natus quo
                                        similique tempore! A asperiores distinctio quaerat repellendus rerum sunt.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore maiores,
                                        ullam. Commodi dolorum est non quae reprehenderit, vero voluptatem. Maiores!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png"
                                         data-img-width="241" data-img-height="309" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores autem
                                        doloribus hic incidunt nam nisi obcaecati praesentium quos veritatis voluptas!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet fuga
                                        laudantium odio possimus quam quis unde veniam. Accusamus, laboriosam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, eaque fugiat
                                        harum libero neque nihil reiciendis sed sunt! Delectus, sit.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos eaque
                                        error est ipsa laboriosam molestias nam quisquam reiciendis similique veniam?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                         data-img-width="241" data-img-height="241" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cupiditate
                                        eius esse expedita harum itaque magni minima, nihil quae quod!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto atque
                                        aut dicta dolore dolorum ex optio provident quae similique.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem beatae, delectus
                                        dolorem esse facilis molestias obcaecati quam qui quia tenetur.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                        illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                        excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                        eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                        impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                        nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                        minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                        cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                        aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                        ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                        dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                        dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                        repellendus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                        officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                        necessitatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                        dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                        error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                        culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                        consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                        quisquam vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                        magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                        cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                        aut corporis maxime nemo non odio qui ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                        cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                        dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                        Unde.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                        doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                        deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                        officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                        soluta. A accusamus accusantium cum eos saepe sed similique.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                        delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                        nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                        iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                        voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                        veritatis?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                        eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                    </div>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="d-none" data-target="fonts">
                    <div class="d-flex align-items-center fonts-filters-container">
                        <select class="custom-select fonts-filters__number-of-products">
                            <option value="50" selected>50</option>
                            <option value="1">10</option>
                            <option value="2">20</option>
                            <option value="3">30</option>
                        </select>
                        <div class="fonts-filters__sample-text-container">
                            <input type="text" placeholder="Type your text">
                            <select class="custom-select">
                                <option value="text 1">text 1</option>
                                <option value="text 2">text 2</option>
                                <option value="text 3">text 3</option>
                                <option value="text 4">text 4</option>
                            </select>
                        </div>
                        <div class="d-flex align-items-center">
                            <img src="assets/images/font-size.svg" class="fonts-filters__font-size-icon">
                            <input type="range" min="1" max="100" value="50" step="1"
                                   class="fonts-filters__range-slider">
                        </div>
                        <label class="d-flex align-items-center">
                            <input type="checkbox" class="show-variants-checkbox">
                            Show variants
                            <img src="assets/images/Info.svg" class="show-variants-icon">
                        </label>
                        <label class="commercial-checkbox-container my-0">
                            <input type="checkbox">
                            Only commercial use license
                        </label>
                        <button class="d-flex align-items-center fonts-filters__reset-btn">
                            <img src="assets/images/reset.svg" class="fonts-filters__info-icon">
                            Reset
                        </button>
                    </div>

                    <div class="articles-container articles-container-1 font-articles-container">
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/panchang.svg" class="js-lazy" data-img-width="344"
                                         data-img-height="106" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">2 styles</span>
                                        <span>Personal and commercial use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira.svg" class="js-lazy" data-img-width="119"
                                         data-img-height="70" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/mystic-crystal.svg" class="js-lazy"
                                         data-img-width="494" data-img-height="107" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">12 Styles</span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue.svg" class="js-lazy" data-img-width="547"
                                         data-img-height="132" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue2.svg" class="js-lazy" data-img-width="505"
                                         data-img-height="128" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira2.svg" class="js-lazy" data-img-width="238"
                                         data-img-height="116" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                    </div>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                </div>
                <div class="articles-container articles-container-7 masonry-container brand-fonts-articles-container d-none"
                     data-target="brand-fonts">
                    <div class="gutter-sizer"></div>
                    <article class="masonry-item pin">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/laliga.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item pin">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/chelsea.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                </div>
                <div class="blog__articles-container blogs__main blogs__sm-container m-auto d-none" data-target="blog">
                    <article class="pin">
                        <a href="" class="d-flex align-items-start flex-column flex-md-row">
                            <div class="article__img-container">
                                <div class="article__img">
                                    <img data-src="assets/images/blog-img.png" class="js-lazy" data-calc-ratio="false">
                                </div>
                            </div>
                            <div class="blog__article__texts-container">
                                <div>
                                    <span class="blogs__article__category">Inspiration</span>
                                    <span class="blogs__article__reading-time">4 min read</span>
                                </div>
                                <h3 class="blog__article__title">Want to improve your web design skills? Start using a
                                    CRO
                                    tool</h3>
                            </div>
                        </a>
                        <img src="assets/images/pin.svg" class="article__pin">
                    </article>
                    <article class="pin">
                        <a href="" class="d-flex align-items-start flex-column flex-md-row">
                            <div class="article__img-container">
                                <div class="article__img">
                                    <img data-src="assets/images/blog-img-2.png" class="js-lazy"
                                         data-calc-ratio="false">
                                </div>
                            </div>
                            <div class="blog__article__texts-container">
                                <div>
                                    <span class="blogs__article__category">Inspiration</span>
                                    <span class="blogs__article__reading-time">4 min read</span>
                                </div>
                                <h3 class="blog__article__title">Product Packaging Design Trends for 2022: from
                                    Sustainable
                                    Packaging to Personalization
                                </h3>
                            </div>
                        </a>
                        <img src="assets/images/pin.svg" class="article__pin">
                    </article>
                    <article class="">
                        <a href="" class="d-flex align-items-start flex-column flex-md-row">
                            <div class="article__img-container">
                                <div class="article__img">
                                    <img data-src="assets/images/blog-img-3.png" class="js-lazy"
                                         data-calc-ratio="false">
                                </div>
                            </div>
                            <div class="blog__article__texts-container">
                                <div>
                                    <span class="blogs__article__category">category</span>
                                    <span class="blogs__article__reading-time">4 min read</span>
                                </div>
                                <h3 class="blog__article__title">Want to improve your web design skills? Start using a
                                    CRO
                                    tool
                                </h3>
                            </div>
                        </a>
                        <img src="assets/images/pin.svg" class="article__pin">
                    </article>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                </div>
            </section>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>