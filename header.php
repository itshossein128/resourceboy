<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- build:css -->
    <link rel="stylesheet" href="style.css">
    <!-- endbuild -->
    <style>
        :root {
            --resourceboy-primary-color: #ff6c4a;
            --resourceboy-primary-color_hover: #ff5831;
        }
    </style>
</head>
<body>
<header class="index__header">
    <button class="header-scroll-btn left"></button>
    <button class="header-scroll-btn right"></button>
    <div class="header-dark">
        <div class="d-flex align-items-center">
            <div class="header__logo-container">
                <a href="">
                    <img class="header__logo" src="assets/images/logo.svg" alt="">
                </a>
            </div>
        </div>
        <div class="header-light">
            <nav>
                <ul class="header-light__navigation p-0">
                    <li><a href="#">
                            Fonts
                        </a></li>
                    <li><a href="#">
                            Mockups
                        </a></li>
                    <li><a href="#">
                            Templates
                        </a></li>
                    <li><a href="#">
                            Textures
                        </a></li>
                    <li><a href="#">
                            Add-Ones
                        </a></li>
                    <li><a href="#">
                            Blog
                        </a></li>
                    <li class="menu-item-has-children">
                        <a href="#">
                            <img src="assets/images/menu-icon-dots.svg" alt="">
                        </a>
                        <ul class="sub-menu" style="display: none">
                            <li><a href="#">About</a></li>
                            <li><a href="#">Brand</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Collections</a></li>
                            <li><a href="#">Help Center</a></li>
                            <li><a href="#">Licenses</a></li>
                            <li><a href="#">Log in</a> <span class="menu__slash">/</span> <a href="#">Sign up</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="header-mobile">
        <div class="d-flex align-items-center justify-content-between">
            <div class="header-mobile__left-side">
                <div class="header-mobile__logo">
                    <a href="">
                        <img src="assets/images/mobile-size-header-logo.svg" class="">
                    </a>
                </div>
            </div>
            <div class="header-mobile__right-side">
                <button class="header-mobile__menu-icon p-0">
                    <img src="assets/images/menu.svg" alt="">
                    <img class="d-none" src="assets/images/close.svg" alt="">
                </button>
                <ul class="js-header-mobile__menu header-mobile__menu" style="display: none">
                    <li><a href="#">About</a></li>
                    <li><a href="#">Brand</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Collections</a></li>
                    <li><a href="#">Help Center</a></li>
                    <li><a href="#">Licenses</a></li>
                    <li><a href="#">Log in</a> <span class="menu__slash">/</span> <a href="#">Sign up</a></li>
                </ul>
            </div>
        </div>
        <div class="swiper-slider header__categories-slider">
            <ul class="header-light__navigation p-0 swiper-wrapper">
                <li class="swiper-slide"><a href="#">
                        Fonts
                    </a></li>
                <li class="swiper-slide"><a href="#">
                        Mockups
                    </a></li>
                <li class="swiper-slide"><a href="#">
                        Templates
                    </a></li>
                <li class="swiper-slide"><a href="#">
                        Textures
                    </a></li>
                <li class="swiper-slide"><a href="#">
                        Add-Ones
                    </a></li>
                <li class="swiper-slide"><a href="#">
                        Blog
                    </a></li>
            </ul>
            <div class="header__categories-slider__button-next swiper-button-next"></div>
            <div class="header__categories-slider__button-prev swiper-button-prev"></div>
        </div>

    </div>
</header>
