<?php include 'header2.php'; ?>
    <div class="container-fluid main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>
        <main class="brand-fonts-main">
            <div class="d-flex align-items-start flex-column flex-lg-row justify-content-between">
                <div class="single-page__img-container brand-font-single__img-container">
                    <div class="mockup-single__img__btns-container">
                        <div class="position-relative share-icon">
                            <div class="single-page__share-btn-container">
                                <img data-src="assets/images/share.svg" class="js-lazy">
                            </div>
                            <div class="share-btns-container">
                                <div class="d-flex align-items-center justify-content-between">
                                    <button>
                                        <img data-src="assets/images/facebook-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/twitter-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/pinterest-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/email-share-btn.svg" class="js-lazy">
                                    </button>
                                </div>
                                <div>
                                    <div class="mockup-single__copy-link">
                                        https://resourceboy.com/photo
                                    </div>
                                    <div class="mockup-single__copied-icon">
                                        <img data-src="assets/images/copy.svg" class="js-lazy">
                                        <img src="assets/images/copied.svg" class="d-none">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-page__comment-btn-container" data-scroll-to="commentsSection">
                            <img src="assets/images/comment.svg" alt="">
                            <span>3</span>
                        </div>
                    </div>
                    <div class="d-flex flex-column flex-xl-row align-items-center justify-content-around brand-font-single__main-images">
                        <div class="position-relative">
                            <img data-src="assets/images/gillette.png" class="img-fluid js-lazy">
                            <a class="share-w-pinterest"><img data-src="assets/images/mockup-single-pinterest.svg"
                                                              class="js-lazy"></a>
                        </div>
                        <div class="position-relative">
                            <img data-src="assets/images/gillette.png" class="img-fluid js-lazy">
                            <a class="share-w-pinterest"><img data-src="assets/images/mockup-single-pinterest.svg"
                                                              class="js-lazy"></a>
                        </div>
                    </div>
                </div>
                <div class="single-page__desc brand-font-single__desc single-page__desc">
                    <div class="d-flex align-items-center justify-content-between mb-3">
                        <div class="font-brand-single__font-name">
                            <h1>Gillete Font</h1>
                            <img data-src="assets/images/green-circle-check.svg" class="ms-2 js-lazy">
                        </div>
                        <div class="d-flex align-items-center">
                            <button class="single-page__desc__comment"><img src="assets/images/comment.svg" alt="">
                            </button>
                            <div class="position-relative mobile-size-share-btns-icon">
                                <img data-src="assets/images/mobile-size-share-icon.svg" class="js-lazy">
                                <div class="share-btns-container mobile-size-share-btns-container">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <button>
                                            <img data-src="assets/images/facebook-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/twitter-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/pinterest-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/email-share-btn.svg" class="js-lazy">
                                        </button>
                                    </div>
                                    <div>
                                        <div class="mockup-single__copy-link">
                                            https://resourceboy.com/photo
                                        </div>
                                        <div class="mockup-single__copied-icon">
                                            <img data-src="assets/images/copy.svg" class="js-lazy">
                                            <img src="assets/images/copied.svg" class="d-none">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="single-page__desc__bookmark"><img src="assets/images/bookmark.svg" alt="">
                            </button>
                            <div class="single-page__report">
                                <img src="assets/images/single-page-menu.svg" alt="">
                                <button>Report</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <table>
                            <tr>
                                <td>Identified Font:</td>
                                <td>ITC Avant Garde <img src="assets/images/Info.svg" class="ms-1"></td>
                            </tr>
                        </table>
                        <div class="fake-table">
                            <span>License:</span>
                            <span>Commercial use</span>
                        </div>
                        <div class="d-flex flex-column">
                            <button class="go-to-free-dl-cm position-relative"><img
                                        data-src="assets/images/creative-market-btn.svg"
                                        class="go-to-free-dl__cm-icon js-lazy">Go to
                                Free Download →
                            </button>
                            <div class="brand-font-single__desc__alternative-font">
                                <div>or an alternative free font that is most similar</div>
                            </div>
                            <button class="go-to-free-dl">Go to Free Download ↓</button>
                        </div>
                        <p class="closed">
                            Selection of <a href="#">iPhone mockups</a> in frontal, perspective, isometric and other
                            angles and
                            materials like clay, realistic and simple shapes in PSD, PNG and Sketch format. Whether
                            you’re a student designing an app, or someone who has dreamed of their own project from the
                            ground up — it’s time to get serious about this. We’ve compiled some iPhone mockups for your
                            convenience! It includes minimalistic simplified/clay style as well as realistic and flat
                            design in different angles including front-facing perspective and isometric ones.
                        </p>
                        <button class="single-page__desc__show-more">Show more</button>
                    </div>
                </div>
            </div>
            <section class="articles d-flex">
                <h2 class="d-flex align-items-center justify-content-between similar-mockups__heading">
                    Similar Mockups
                </h2>
                <div class="articles-container articles-container-7 masonry-container brand-fonts-articles-container">
                    <?php include 'articles-loading.php' ?>
                    <div class="gutter-sizer"></div>
                    <article class="masonry-item pin">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/laliga.png" data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item pin">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/chelsea.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative brand-font__article__border">
                            <a href="" class="article__img" style="padding-top: 66.78%">
                                <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                     data-calc-ratio="false">
                                <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                </div>
                <button class="mockup-single__load-more"><span>Load More</span></button>

                <div class="d-flex justify-content-center flex-column align-items-center mockup-single__explore-related-categories">
                    <span class="">Explore Related Categories</span>
                    <div class="single-page__related-categories">
                        <a href="#">Brochure Mockups</a>
                        <a href="#">Business Card Mockups</a>
                        <a href="#">Stationery Mockups</a>
                        <a href="#">Paper Mockups</a>
                    </div>
                </div>
            </section>
            <section class="comments-section container" id="commentsSection">
                <span class="comments-section__header">7 comments </span>
                <div class="comments-container">
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container no-pic-avatar">
                                    <img data-src="assets/images/no-pic-avatar.svg" class="js-lazy">
                                </div>
                                <span>Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <span class="comment-section__title">Leave a Comment</span>
                    <span class="comment-section__desc">Your email address will not be published. Required fields are marked *</span>
                    <form class="leave-a-comment-form row">
                        <label>
                            <textarea name="comment" id="" cols="30" rows="10" placeholder="Your comment *"
                                      class="col-12"></textarea>
                        </label>
                        <label>
                            <input type="text" placeholder="Your name *" name="name" class="col-12">
                        </label>
                        <label>
                            <input type="text" placeholder="Your email address *" name="email" class="col-12">
                        </label>
                        <div>
                            <button class="">Submit your comment →</button>
                        </div>
                    </form>
                </div>
            </section>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>