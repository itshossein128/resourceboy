<?php include 'header2.php'; ?>
    <div class="container-fluid main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>

        <main class="brand-fonts-main">
            <h1 class="main__heading">Brand Fonts</h1>
            <div class="categories-slider swiper">
                <div class="swiper-wrapper">
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                </div>
                <div class="categories-slider__button-next swiper-button-next"></div>
                <div class="categories-slider__button-prev swiper-button-prev"></div>
            </div>
            <div class="d-flex align-items-stretch flex-column flex-sm-row">
                <aside class="aside-search-container d-none d-sm-block">
                    <div>
                        <div class="aside__heading">
                            <img data-src="assets/images/4-squares.svg" class="js-lazy">
                            <span>Mockups</span>
                        </div>
                        <div class="aside__search-field">
                            <label class="d-flex align-items-center">
                                <img data-src="assets/images/search-icon.svg" class="js-lazy">
                                <input type="search" placeholder="Search">
                            </label>
                            <ul>
                                <li><a href="">Apple Device</a></li>
                                <li><a href="">Book</a></li>
                                <li><a href="">Bottle</a></li>
                                <li><a href="">Box</a></li>
                                <li><a href="">Branding</a></li>
                                <li><a href="">Brochure</a></li>
                                <li><a href="">Business Card</a></li>
                                <li><a href="">Device</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                            </ul>
                            <button href="#">+ Show more</button>
                        </div>
                    </div>
                </aside>
                <section class="articles index__articles">
                    <div class="d-flex align-items-center justify-content-between">
                        <div class="d-flex align-items-center justify-content-start">
                            <div>
                                <div class="cards-section__header">
                                    <button class="popular-btn active" data-target="popular">Popular</button>
                                    <button class="newest-btn" data-target="newest">Newest</button>
                                    <div class="have-button-active-effect"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="articles-container articles-container-5-w-aside masonry-container brand-fonts-articles-container"
                         data-open="popular">
                        <?php include 'articles-loading.php' ?>
                        <div class="gutter-sizer"></div>
                        <article class="masonry-item pin brand-font-verified">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/laliga.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item pin">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/chelsea.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item brand-font-verified">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                    </div>
                    <div class="articles-container articles-container-5-w-aside masonry-container brand-fonts-articles-container d-none"
                         data-open="newest">
                        <?php include 'articles-loading.php' ?>
                        <div class="gutter-sizer"></div>
                        <article class="masonry-item pin">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/chelsea.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item pin brand-font-verified">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/laliga.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/hettich.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/manchester-united.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/serie-a.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item brand-font-verified">
                            <div class="position-relative brand-font__article__border">
                                <a href="" class="article__img" style="padding-top: 66.78%">
                                    <img class="js-lazy brand-font-first-pic" alt="" data-src="assets/images/adservio.png"
                                         data-calc-ratio="false">
                                    <img class="js-lazy brand-font-second-pic" alt="" data-src="assets/images/adservio.png" data-calc-ratio="false">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                    <img data-src="assets/images/green-circle-check.svg"
                                         class="ms-2 js-lazy brand-font-verified-icon">
                                </a>
                            </div>
                        </article>
                    </div>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                </section>
            </div>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>