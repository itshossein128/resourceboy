import gulp from 'gulp';
import concat from 'gulp-concat';
import cleanCss from 'gulp-clean-css';
import uglify from 'gulp-uglify';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import purgeCss from 'gulp-purgecss';
import htmlreplace from 'gulp-html-replace';
import autoprefixer from 'autoprefixer';
import sourcemaps from 'gulp-sourcemaps';
import postcss from 'gulp-postcss'
// import checkCss from 'gulp-check-unused-css';

const sass = gulpSass(dartSass);

let autoprefixBrowsers = ["> 1%", "ie >= 8", "edge >= 15", "ie_mob >= 10", "ff >= 40", "chrome >= 40", "safari >= 7", "opera >= 23", "ios >= 7", "android >= 4", "bb >= 10"];

let jsSources = [
    'js/nice-select2.js',
    'js/lazyload.min.js',
    'js/masonry.js',
    'js/readmore.js',
    'js/swiper.js',
    'js/main.js'
]
let sassSource = ['sass/style.scss']
let imagesSource = ['assets/images/*']
let htmlSource = ['*.html']
let phpSource = ['*.php']
let fontSources = ['assets/fonts/*']

gulp.task('js', function (done) {
    gulp.src(jsSources)
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
    done()
})
gulp.task("sass", function (done) {
    gulp.src(sassSource)
        .pipe(sass({style: 'expanded'}))
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer(autoprefixBrowsers)]))
        .pipe(purgeCss({
            content: ['*.php', 'js/*.js', 'popup/*.html', 'sass/libs/_swiper.scss'],
        }))
        .pipe(cleanCss({level: {1: {specialComments: 0}}}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'))
    done()
});
// gulp.task("checkCss", function (done) {
//     return gulp
//         .src(['dist/style.css', 'dist/*.html'])
//         .pipe(checkCss());
// })
gulp.task('images', function (done) {
    gulp.src(imagesSource)
        .pipe(gulp.dest('dist/assets/images'));
    done()
})
gulp.task('html', function (done) {
    gulp.src(htmlSource)
        .pipe(htmlreplace({
            'css': 'style.css',
            'js': 'js/scripts.js'
        }))
        .pipe(gulp.dest('dist'));

    gulp.src(phpSource)
        .pipe(htmlreplace({
            'css': 'style.css',
            'js': 'js/scripts.js'
        }))
        .pipe(gulp.dest('dist'));


    gulp.src('popup/*.html')
        .pipe(htmlreplace({
            'css': 'style.css',
            'js': 'js/scripts.js'
        }))
        .pipe(gulp.dest('dist/popup'));

    done()
})
gulp.task('fonts', function (done) {
    gulp.src(fontSources)
        .pipe(gulp.dest('dist/assets/fonts'));
    done()
})

gulp.task('watch', function () {
    gulp.watch(jsSources, gulp.series('js'));
    gulp.watch('style.css', gulp.series('sass'));
    gulp.watch(imagesSource, gulp.series('images'));
    gulp.watch(htmlSource, gulp.series('html'));
})


//because we don't need to watch task anymore:
// gulp.task('default', gulp.series('js', 'sass', 'images', 'html', 'fonts', 'watch'));
gulp.task('default', gulp.series('js', 'sass', 'images', 'html', 'fonts'));


//header v footer v main joda bashan va ba php be ham bechasban