<?php include 'header2.php'; ?>
    <div class="container-fluid main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>
        <main class="mockup-single-main">
            <div class="d-flex align-items-start flex-column flex-lg-row justify-content-between">
                <div class="single-page__img-container">
                    <div class="mockup-single__img__btns-container">
                        <div class="single-page__bookmark-btn-container">
                            <img src="assets/images/bookmark.svg" alt="">
                        </div>
                        <div class="position-relative share-icon">
                            <div class="single-page__share-btn-container">
                                <img data-src="assets/images/share.svg" class="js-lazy">
                            </div>
                            <div class="share-btns-container">
                                <div class="d-flex align-items-center justify-content-between">
                                    <button>
                                        <img data-src="assets/images/facebook-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/twitter-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/pinterest-share-btn.svg" class="js-lazy">
                                    </button>
                                    <button>
                                        <img data-src="assets/images/email-share-btn.svg" class="js-lazy">
                                    </button>
                                </div>
                                <div>
                                    <div class="mockup-single__copy-link">
                                        https://resourceboy.com/photo
                                    </div>
                                    <div class="mockup-single__copied-icon">
                                        <img data-src="assets/images/copy.svg" class="js-lazy">
                                        <img src="assets/images/copied.svg" class="d-none">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single-page__comment-btn-container" data-scroll-to="commentsSection">
                            <img src="assets/images/comment.svg" alt="">
                            <span>10</span>
                        </div>
                    </div>
                    <div class="swiper single-page__slider">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="position-relative">
                                    <img src="assets/images/flyers-single-img.png" class="img-fluid">
                                    <a class="share-w-pinterest"><img
                                                data-src="assets/images/mockup-single-pinterest.svg"
                                                class="js-lazy"></a>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="position-relative">
                                    <img src="assets/images/flyers-single-img.png" class="img-fluid">
                                    <a class="share-w-pinterest"><img
                                                data-src="assets/images/mockup-single-pinterest.svg"
                                                class="js-lazy"></a>
                                </div>
                            </div>
                        </div>
                        <div class="single-page__slider-button-next"><img src="assets/images/swiper-angel.svg" alt="">
                        </div>
                        <div class="single-page__slider-button-prev"><img src="assets/images/swiper-angel.svg" alt="">
                        </div>
                        <div class="single-page__pagination"></div>
                    </div>
                </div>
                <div class="single-page__desc">
                    <div class="d-flex align-items-center justify-content-between">
                        <a href="#" class="single-page__desc__contributor"><img data-src="assets/images/contributor.svg"
                                                                                class="me-2 js-lazy">
                            <span>Contributor</span></a>
                        <div class="d-flex align-items-center">
                            <div class="single-page__desc__exclusive">
                                <img data-src="assets/images/exclusive.svg" class="js-lazy">
                                <span class="exclusive-badge">Exclusive</span>
                            </div>
                            <button class="single-page__desc__comment" data-scroll-to="commentsSection"><img
                                        src="assets/images/comment.svg" alt=""></button>
                            <div class="position-relative mobile-size-share-btns-icon">
                                <img data-src="assets/images/mobile-size-share-icon.svg" class="js-lazy">
                                <div class="share-btns-container mobile-size-share-btns-container">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <button>
                                            <img data-src="assets/images/facebook-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/twitter-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/pinterest-share-btn.svg" class="js-lazy">
                                        </button>
                                        <button>
                                            <img data-src="assets/images/email-share-btn.svg" class="js-lazy">
                                        </button>
                                    </div>
                                    <div>
                                        <div class="mockup-single__copy-link">
                                            https://resourceboy.com/photo
                                        </div>
                                        <div class="mockup-single__copied-icon">
                                            <img data-src="assets/images/copied.svg" class="js-lazy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="single-page__desc__bookmark"><img src="assets/images/bookmark.svg" alt="">
                            </button>
                            <div class="single-page__report">
                                <img src="assets/images/single-page-menu.svg" alt="">
                                <button>Report</button>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h1 class="single-page__desc__heading">Clean Border Retro Music Event Flyer and Facebook
                            Cover </h1>
                        <table>
                            <tr>
                                <td>License:</td>
                                <td>Commercial use</td>
                            </tr>
                        </table>
                        <div class="fake-table">
                            <span>Compatibility:</span>
                            <span>Adobe Photoshop</span>
                        </div>
                        <div class="d-flex flex-column">
                            <button class="go-to-free-dl">Go to Free Download →</button>
                            <button class="go-to-free-dl-cm position-relative"><img
                                        data-src="assets/images/creative-market-btn.svg"
                                        class="go-to-free-dl__cm-icon js-lazy">Go to
                                Free Download →
                            </button>
                        </div>
                        <p class="closed">
                            Selection of <a href="#">iPhone mockups</a> in frontal, perspective, isometric and other
                            angles and
                            materials like clay, realistic and simple shapes in PSD, PNG and Sketch format. Whether
                            you’re a student designing an app, or someone who has dreamed of their own project from the
                            ground up — it’s time to get serious about this. We’ve compiled some iPhone mockups for your
                            convenience! It includes minimalistic simplified/clay style as well as realistic and flat
                            design in different angles including front-facing perspective and isometric ones.
                        </p>
                        <button class="single-page__desc__show-more">Show more</button>
                    </div>
                </div>
            </div>
            <section class="articles d-flex">
                <div class="d-flex align-items-center justify-content-between">
                    Similar Mockups
                </div>
                <div class="articles-container articles-container-7 masonry-container">
                    <?php include 'articles-loading.php' ?>
                    <div class="gutter-sizer"></div>
                    <article class="masonry-item pin" data-currentid="1" data-nextid="1" data-previd="1">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png" data-img-width="212"
                                     data-img-height="312" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item pin">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-1.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-2.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-4.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-5.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png" data-img-width="212"
                                     data-img-height="312" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-7.png" data-img-width="212"
                                     data-img-height="272" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                    illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="212"
                                     data-img-height="212" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                    excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-1.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                    eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-4.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                    impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="212"
                                     data-img-height="212" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                    nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-2.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                    minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-7.png" data-img-width="212"
                                     data-img-height="272" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                    cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                    aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                    ullam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                    dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                    dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                    repellendus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                    officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                    necessitatibus!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                    dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                    error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                    culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                    consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                    quisquam vero.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                    magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                    cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                    aut corporis maxime nemo non odio qui ullam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                    cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                    dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                    Unde.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                    doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                    deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                    officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                    soluta. A accusamus accusantium cum eos saepe sed similique.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                    delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                    nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                    iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                    voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                    veritatis?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="212"
                                     data-img-height="142" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                    eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a class="articleLink" href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                </div>
                <button class="mockup-single__load-more"><span>Load More</span></button>

                <div class="d-flex justify-content-center flex-column align-items-center mockup-single__explore-related-categories">
                    <span class="">Explore Related Categories</span>
                    <div class="single-page__related-categories">
                        <a href="#">Brochure Mockups</a>
                        <a href="#">Business Card Mockups</a>
                        <a href="#">Stationery Mockups</a>
                        <a href="#">Paper Mockups</a>
                    </div>
                </div>
            </section>
            <section class="comments-section container" id="commentsSection">
                <span class="comments-section__header">7 comments </span>
                <div class="comments-container">
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container no-pic-avatar">
                                    <img data-src="assets/images/no-pic-avatar.svg" class="js-lazy">
                                </div>
                                <span>Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <span class="comment-section__title">Leave a Comment</span>
                    <span class="comment-section__desc">Your email address will not be published. Required fields are marked *</span>
                    <form class="leave-a-comment-form row">
                        <label>
                            <textarea name="comment" id="" cols="30" rows="10" placeholder="Your comment *"
                                      class="col-12"></textarea>
                        </label>
                        <label>
                            <input type="text" placeholder="Your name *" name="name" class="col-12">
                        </label>
                        <label>
                            <input type="text" placeholder="Your email address *" name="email" class="col-12">
                        </label>
                        <div>
                            <button class="">Submit your comment →</button>
                        </div>
                    </form>
                </div>
            </section>
        </main>
        <div class="popup-container">
            <div class="popup-loading-container">
                <div class="popup-loading"></div>
            </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>