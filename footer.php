</main>
<footer class="footer">
    <div class="footer__texts-container">
        <p>Love is everything. We made Resource Boy with the utmost <span class="footer__heart-container">♥</span> love.
        </p>
        <p class="footer__copyright">All resources © of their respective owners.</p>
    </div>
    <nav>
        <ul class="m-0">
            <li><a href="#">Home</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
    </nav>
</footer>
<!-- build:js -->
<script src="js/readmore.js"></script>
<script src="js/nice-select2.js"></script>
<script src="js/swiper.js"></script>
<script src="js/masonry.js"></script>
<script src="js/lazyload.min.js"></script>
<script src="js/main.js"></script>
<!-- endbuild -->
</body>
</html>