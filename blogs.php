<?php include 'header2.php'; ?>
    <main class="container blogs__main">
        <div class="blogs__sm-container m-auto">
            <h1 class="blog__main__heading">RB Blog</h1>
            <div class="blogs-categories-container">
                <a href="#">All</a>
                <a href="#">Design Basics</a>
                <a href="#">Resources</a>
                <a href="#">Inspiration</a>
                <a href="#">Trends</a>
                <a href="#">Interviews</a>
            </div>
            <section class="blog__articles-container">
                <article class="pin">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="article__img-container">
                            <a class="article__img">
                                <img data-src="assets/images/blog-img.png" class="js-lazy" data-calc-ratio="false">
                            </a>
                        </div>
                        <div class="blog__article__texts-container">
                            <div class="position-relative">
                                <a href="#" class="blogs__article__category">Inspiration</a>
                                <span class="blogs__article__reading-time">4 min read</span>
                                <img src="assets/images/pin.svg" class="article__pin">
                            </div>
                            <h2 class="blog__article__title"><a href="">Want to improve your web design skills? Start
                                    using a CRO
                                    tool</a></h2>
                        </div>
                    </div>
                </article>
                <article class="pin">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="article__img-container">
                            <a href="#" class="article__img">
                                <img data-src="assets/images/blog-img-2.png" class="js-lazy" data-calc-ratio="false">
                            </a>
                        </div>
                        <div class="blog__article__texts-container">
                            <div class="position-relative">
                                <a href="#" class="blogs__article__category">Inspiration</a>
                                <span class="blogs__article__reading-time">4 min read</span>
                                <img src="assets/images/pin.svg" class="article__pin">
                            </div>
                            <h2 class="blog__article__title"><a href="#">Product Packaging Design Trends for 2022: from
                                    Sustainable
                                    Packaging to Personalization</a>
                            </h2>
                        </div>
                    </div>
                </article>
                <article class="">
                    <div class="d-flex align-items-start flex-column flex-md-row">
                        <div class="article__img-container">
                            <a href="#" class="article__img">
                                <img data-src="assets/images/blog-img-3.png" class="js-lazy" data-calc-ratio="false">
                            </a>
                        </div>
                        <div class="blog__article__texts-container">
                            <div class="position-relative">
                                <a href="#" class="blogs__article__category">category</a>
                                <span class="blogs__article__reading-time">4 min read</span>
                                <img src="assets/images/pin.svg" class="article__pin">
                            </div>
                            <h2 class="blog__article__title"><a href="#">Want to improve your web design skills? Start
                                    using a CRO
                                    tool</a>
                            </h2>
                        </div>
                    </div>
                </article>
                <nav class="navigation pagination" role="navigation" aria-label=" ">
                    <div class="nav-links">
                        <a class="prev page-numbers d-none" href="#">
                            <span class="pagination__arrow-container">→</span>
                            Prev
                        </a>
                        <div class="scroll-horizontally">
                            <a class="page-numbers current" href="#">1</a>
                            <a class="page-numbers" href="#">2</a>
                            <a class="page-numbers" href="#">3</a>
                            <span class="page-numbers more">...</span>
                            <a class="page-numbers" href="#">196</a>
                        </div>
                        <a class="next page-numbers" href="#">
                            Next
                            <span class="pagination__arrow-container">→</span>
                        </a>
                    </div>
                </nav>
            </section>
        </div>
    </main>
<?php include 'footer.php'; ?>