<?php include 'header2.php'; ?>
    <div class="container-fluid main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>

        <main class="flyers-main">
            <h1 class="main__heading">Branding Mockups</h1>
            <div class="categories-slider swiper">
                <div class="swiper-wrapper">
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                </div>
                <div class="categories-slider__button-next swiper-button-next"></div>
                <div class="categories-slider__button-prev swiper-button-prev"></div>
            </div>
            <div class="d-flex align-items-stretch flex-column flex-sm-row">
                <aside class="aside-search-container d-none d-sm-block">
                    <div>
                        <div class="aside__heading">
                            <img data-src="assets/images/4-squares.svg" class="js-lazy">
                            <span>Mockups</span>
                        </div>
                        <div class="aside__search-field">
                            <label class="d-flex align-items-center">
                                <img data-src="assets/images/search-icon.svg" class="js-lazy">
                                <input type="search" placeholder="Search">
                            </label>
                            <ul>
                                <li><a href="">Apple Device</a></li>
                                <li><a href="">Book</a></li>
                                <li><a href="">Bottle</a></li>
                                <li><a href="">Box</a></li>
                                <li><a href="">Branding</a></li>
                                <li><a href="">Brochure</a></li>
                                <li><a href="">Business Card</a></li>
                                <li><a href="">Device</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                            </ul>
                            <button href="#">+ Show more</button>
                        </div>
                    </div>
                </aside>
                <section class="articles index__articles">
                    <div class="d-flex align-items-center justify-content-between flex-wrap row-gap-10">
                        <div class="d-flex align-items-center justify-content-start">
                            <div>
                                <div class="cards-section__header">
                                    <button class="popular-btn active" data-target="popular">Popular</button>
                                    <button class="newest-btn" data-target="newest">Newest</button>
                                    <div class="have-button-active-effect"></div>
                                </div>
                            </div>
                            <div class="commercial-use-badge d-none d-lg-block">
                                Commercial Use
                                <button><img data-src="assets/images/close-icon.svg" class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                            <label class="commercial-checkbox-container">
                                <input type="checkbox">
                                Only commercial use license
                                <img data-src="assets/images/Info.svg" class="js-lazy">
                            </label>
                        </div>
                    </div>
                    <div class="articles-container articles-container-5-w-aside masonry-container" data-open="popular">
                        <?php include 'articles-loading.php' ?>
                        <div class="gutter-sizer"></div>
                        <article class="masonry-item pin">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item pin">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                        illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                        excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                        eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                        impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                        nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                        minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                        cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                        aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                        ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                        dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                        dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                        repellendus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                        officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                        necessitatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                        dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                        error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                        culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                        consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                        quisquam vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                        magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                        cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                        aut corporis maxime nemo non odio qui ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                        cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                        dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                        Unde.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                        doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                        deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                        officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                        soluta. A accusamus accusantium cum eos saepe sed similique.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                        delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                        nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                        iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                        voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                        veritatis?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                        eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                    </div>
                    <div class="articles-container articles-container-5-w-aside masonry-container d-none"
                         data-open="newest">
                        <?php include 'articles-loading.php' ?>
                        <div class="gutter-sizer"></div>
                        <article class="masonry-item pin">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item pin">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                        explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                        deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                        hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                        illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                        excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                        eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                        impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                        nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                        minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                        cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                        aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                        ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                        dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                        dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                        repellendus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                        officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                        necessitatibus!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                        dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                        error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                        culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                        consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                        quisquam vero.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                        magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                        cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                        aut corporis maxime nemo non odio qui ullam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                        cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                        dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                        Unde.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                        doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                        deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                        officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                        soluta. A accusamus accusantium cum eos saepe sed similique.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                        delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                        nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                        iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                        voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                        veritatis?
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                        <article class="masonry-item">
                            <div class="position-relative">
                                <a href="" class="article__img">
                                    <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                         data-img-width="342" data-img-height="229" data-calc-ratio="true">
                                </a>
                                <div class="versions-number">
                                    <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                    <span>12</span>
                                    <div class="versions__desc">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                        eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                                    </div>
                                </div>
                            </div>
                            <div class="article__caption">
                                <a href="">
                                    <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                    <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                                </a>
                            </div>
                        </article>
                    </div>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                    <div class="flyers__desc closed">
                        <h2 class="flyers__desc__heading">Heading Two</h2>
                        <p>Selection of iPhone mockups in frontal, perspective, isometric and other angles and materials
                            like clay, realistic and simple shapes in PSD, PNG and Sketch format. Whether you’re a
                            student designing an app, or someone who has dreamed of their own project from the ground up
                            — it’s time to get serious about this. We’ve compiled some iPhone mockups for your
                            convenience! It includes minimalistic simplified/clay style as well as realistic and flat
                            design in different angles including front-facing perspective and isometric ones. Lorem
                            ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto aspernatur cum
                            cupiditate deleniti distinctio dolores eligendi expedita fuga fugit impedit iste libero
                            minus necessitatibus nihil odio, optio pariatur perferendis praesentium quasi quibusdam quo
                            rem sequi, suscipit vero vitae voluptas. Ab architecto, cumque deleniti dolores magni, natus
                            obcaecati provident reiciendis, repellat sed sint temporibus vitae voluptas? Dolorum ea est
                            sapiente tenetur unde voluptate. Distinctio fugit labore laborum quidem quisquam ut vitae?
                            Amet asperiores assumenda beatae distinctio dolorum ea enim eum hic iste labore laudantium,
                            nostrum nulla numquam odio perspiciatis temporibus tenetur. Dolores dolorum excepturi
                            facilis fugit quasi ullam unde ut!</p>
                        <button>Show more</button>
                    </div>
                </section>
            </div>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>