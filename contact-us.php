<?php include 'header2.php'; ?>
    <main class="container contact-us-main">
        <h1 class="contact-us__heading">Contact Us</h1>
        <div class="contact-us-desc">
            <p>Drop us a message with your idea, suggestion, or question. We'll get back to you as soon as we can.
            </p>
            <p>Please note that we're not currently looking for new writers, or accepting guest posts. We'd be happy to
                work with you on a piece of sponsored content to share with our audience.</p>
        </div>
        <form class="row mx-0">
            <label class="p-0">
                <input class="col-12" type="text" placeholder="Your name *" name="name">
            </label>
            <label class="p-0">
                <input class="col-12" type="text" placeholder="Your email address *" name="email">
            </label>
            <label class="p-0">
                <input class="col-12" type="text" placeholder="Subject *" name="subject">
            </label>
            <textarea class="col-12" name="" id="" cols="30" rows="6" placeholder="Your comment *"></textarea>
            <button class="send-message">Send message →</button>
        </form>
    </main>
<?php include 'footer.php'; ?>