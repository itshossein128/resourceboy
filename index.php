<?php include 'header.php'; ?>
    <main class="main-content-container index__main-container">
        <section class="index__resource-section container index__search-section">
            <h1>The best free design resources around the world are here</h1>
            <form class="search-box" action="">
                <label class="search-box__input">
                    <input type="search" placeholder="Search...">
                </label>
                <label class="search-box__button">
                    <button class="search-icon"></button>
                </label>
            </form>
        </section>
        <section class="RB-exclusives">
            <div class="RB-exclusives__heading">
                <span class="RB-exclusives__titre">Resource Boy Exclusives</span>
                <a href="#">View All</a>
            </div>
            <div class="RB-exclusives__cards-container">
                <article class="RB-card">
                    <a href="">
                        <div>
                            <a href="" class="article__img" style="padding-top: 67%">
                                <img class="js-lazy img-fluid w-100" data-src="assets/images/RB-img-3.png" style="position: absolute; top: 0; left: 0; width: 100%">
                            </a>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <h2>100 Watercolor Photoshop Brushes</h2>
                            </a>
                        </div>
                    </a>
                </article>
                <article class="RB-card">
                    <a href="">
                        <div>
                            <a href="" class="article__img" style="padding-top: 67%">
                                <img class="js-lazy img-fluid w-100" data-src="assets/images/RB-img-1.png" style="position: absolute; top: 0; left: 0; width: 100%">
                            </a>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <h2>100 Watercolor Photoshop Brushes</h2>
                            </a>
                        </div>
                    </a>
                </article>
                <article class="RB-card">
                    <a href="">
                        <div>
                            <a href="" class="article__img" style="padding-top: 67%">
                                <img class="js-lazy img-fluid w-100" data-src="assets/images/RB-img-5.png" style="position: absolute; top: 0; left: 0; width: 100%">
                            </a>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <h2>100 Watercolor Photoshop Brushes</h2>
                            </a>
                        </div>
                    </a>
                </article>
                <article class="RB-card">
                    <a href="">
                        <div>
                            <a href="" class="article__img" style="padding-top: 67%">
                                <img class="js-lazy img-fluid w-100" data-src="assets/images/RB-img-3.png" style="position: absolute; top: 0; left: 0; width: 100%">
                            </a>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <h2>100 Watercolor Photoshop Brushes</h2>
                            </a>
                        </div>
                    </a>
                </article>
                <article class="RB-card d-none d-xl-block">
                    <a href="">
                        <div>
                            <a href="" class="article__img" style="padding-top: 67%">
                                <img class="js-lazy img-fluid w-100" data-src="assets/images/RB-img-2.png" style="position: absolute; top: 0; left: 0; width: 100%">
                            </a>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <h2>100 Watercolor Photoshop Brushes</h2>
                            </a>
                        </div>
                    </a>
                </article>
            </div>
            <hr>
        </section>
        <section class="articles index__articles">
            <span class="articles-container__heading">Newest Resources</span>
            <div class="articles-container articles-container-7 masonry-container">
                <?php include 'articles-loading.php' ?>
                <div class="gutter-sizer"></div>
                <article class="masonry-item pin have-version-number" data-currentid="1" data-nextid="1"
                         data-previd="1">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png"
                                 data-img-width="212"
                                 data-img-height="312" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Resource Boy is the world’s leading community for creatives to share, grow, and get hired.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item pin">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-1.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Resource Boy is the world’s leading community for creatives to share, grow, and get hired.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item pin">
                    <div class="position-relative">
                        <a href="" class="article__img have-border">
                            <img class="js-lazy" alt="" data-src="assets/images/img-bg-white.jpg" data-img-width="600"
                                 data-img-height="400" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque dicta harum id
                                iste nesciunt perspiciatis quae repellendus ullam ut voluptates.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item have-version-number">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-2.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Resource Boy is the world’s leading community for creatives to share, grow, and get hired.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-4.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Resource Boy is the world’s leading community for creatives to share, grow, and get hired.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-5.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png"
                                 data-img-width="212"
                                 data-img-height="312" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-7.png"
                                 data-img-width="212"
                                 data-img-height="272" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                 data-img-width="212" data-img-height="212" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-1.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-4.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png"
                                 data-img-width="212" data-img-height="212" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-2.png"
                                 data-img-width="212"
                                 data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-7.png"
                                 data-img-width="212"
                                 data-img-height="272" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                ullam.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                repellendus.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                necessitatibus!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                quisquam vero.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                aut corporis maxime nemo non odio qui ullam.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                Unde.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                soluta. A accusamus accusantium cum eos saepe sed similique.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                veritatis?
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
                <article class="masonry-item">
                    <div class="position-relative">
                        <a href="" class="article__img">
                            <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png"
                                 data-img-width="212" data-img-height="142" data-calc-ratio="true">
                        </a>
                        <div class="versions-number">
                            <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                            <span>12</span>
                            <div class="versions__desc">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                            </div>
                        </div>
                    </div>
                    <div class="article__caption">
                        <a class="articleLink" href="">
                            <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                            <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                        </a>
                    </div>
                </article>
            </div>
            <nav class="navigation pagination" role="navigation" aria-label=" ">
                <div class="nav-links">
                    <a class="prev page-numbers d-none" href="#">
                        <span class="pagination__arrow-container">→</span>
                        Prev
                    </a>
                    <div class="scroll-horizontally">
                        <a class="page-numbers current" href="#">1</a>
                        <a class="page-numbers" href="#">2</a>
                        <a class="page-numbers" href="#">3</a>
                        <span class="page-numbers more">...</span>
                        <a class="page-numbers" href="#">196</a>
                    </div>
                    <a class="next page-numbers" href="#">
                        Next
                        <span class="pagination__arrow-container">→</span>
                    </a>
                </div>
            </nav>
        </section>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </main>
<?php include 'footer.php'; ?>