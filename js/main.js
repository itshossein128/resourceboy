//background color of header search label start
if (document.querySelector('.js-header-search-input')) {
    document.querySelector('.js-header-search-input').addEventListener('focus', function () {
        document.querySelector('.js-header-label').classList.add('bg-white')
    })
    document.querySelector('.js-header-search-input').addEventListener('focusout', function () {
        document.querySelector('.js-header-label').classList.remove('bg-white')
    })
}
//background color of header search label end

///////////////////////////////////////////////////////////////////
//submenu start
if (document.querySelector('.menu-item-has-children')) {
    document.querySelector('.menu-item-has-children a').addEventListener('mouseenter', function () {
        this.parentElement.querySelector('.sub-menu').classList.add('show')

    })
    document.querySelector('.menu-item-has-children').addEventListener('mouseleave', function () {
        this.querySelector('.sub-menu').classList.remove('show')
    })
}
//submenu end
///////////////////////////////////////////////////////////////////
// search mobile start
if (document.querySelector('.js-search-close-container')) {
    document.querySelector('.js-search-close-container').addEventListener('click', function () {
        const searchFormContainer = document.querySelector('.js-header-mobile__search');
        const searchForm = document.querySelector('.js-header-mobile__search form');
        const buttonIcons = [...this.children];
        document.querySelector('.js-mobile-header-search-input').focus()
        searchFormContainer.classList.toggle('hide')
        searchForm.classList.toggle('hide')
        buttonIcons.forEach(item => item.classList.toggle('d-none'))
    })
}
// search mobile end
//////////////////////////////////////////////////////////////////

document.querySelector('.header-mobile__menu-icon').addEventListener('click', function () {
    const buttonIcons = [...this.children]
    const mobileMenu = document.querySelector('.js-header-mobile__menu')
    buttonIcons.forEach(item => item.classList.toggle('d-none'))
    document.querySelector('.js-header-mobile__menu').classList.toggle('show')
});

// document.onreadystatechange = () => {
//     if (document.readyState === 'complete') {
//     document.querySelector('.js-header-mobile__menu').style.height = document.querySelector('.js-header-mobile__menu').scrollHeight + 5 + 'px'
//     }
// };


/* LazyLoad --> */
if (document.querySelector('img[data-calc-ratio]')) {
    document.querySelectorAll('img[data-calc-ratio]').forEach(function (img, imgNumber,) {
        if (img.dataset.calcRatio === 'true') {
            img.parentElement.style.paddingTop = img.dataset.imgHeight * 100 / img.dataset.imgWidth + '%'
        }
        img.parentElement.classList.add('position-relative')
        img.classList.add('img-with-placeholder')
        if (imgNumber + 1 === document.querySelectorAll('img[data-calc-ratio]').length) {
            removeArticlesLoading()
        }
    })
}

function imageIsLoading(img) {

}

function imageLoaded(img) {

    if (img.parentElement.classList.contains('article__img')) {
        img.parentElement.style.background = '#FFFFFF'
    }
}

const lazyLoadInstance = new LazyLoad({
    elements_selector: '.js-lazy',
    load_delay: 1000,
    threshold: 200,
    callback_loaded: imageLoaded,
    callback_enter: enterLazyLoadedImages,
    callback_loading: imageIsLoading,
});
/* LazyLoad <-- */

// mobile menu sub menus start
if (document.querySelector('.js-header-mobile__menu')) {
    document.querySelectorAll('.js-header-mobile__menu>ul>li.menu-item-has-children').forEach(item => {
        item.addEventListener('click', function () {
            item.style.maxHeight = item.scrollHeight + 'px';
            this.classList.toggle('is-open')
        })
    })
}
// mobile menu sub menus end

//////////////////////////////////////////////////////////////
// show more button single page start
let counter = 0;
let readMore;
window.addEventListener('resize', registerShowMoreAction);

function registerShowMoreAction() {

    if (document.querySelectorAll('.single__main__header__desc')) {
        document.querySelectorAll('.single__main__header__desc').forEach(function (value, index) {
            if (value.getAttribute('data-rendered') != 'false') {
                readMore = new Readmore(value, {
                    speed: 100,
                    afterToggle: function (trigger, element, expanded) {
                    },
                    beforeToggle: function (trigger, element, expanded) {
                        if (expanded) {
                            value.dataset.resize = true;
                            if (document.querySelector('.popup-container').classList.contains('show-popup')) {
                                document.querySelector('.popup-container').scrollTop = 0;
                            } else {
                                document.body.scrollTop = 0; // For Safari
                                document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                            }
                        }
                    }
                });
                value.setAttribute('data-rendered', 'false');
            }
        });

        document.querySelectorAll('.popup-container .single__main__header__desc').forEach(function (value, index) {
            if (readMore) {
                readMore.destroy();
                document.querySelector('.single__main__header__desc').setAttribute('data-rendered', 'false');
            }

            readMore = new Readmore(value, {
                speed: 100,
                afterToggle: function (trigger, element, expanded) {
                },
                beforeToggle: function (trigger, element, expanded) {
                    if (expanded) {
                        value.dataset.resize = true;
                        if (document.querySelector('.popup-container').classList.contains('show-popup')) {
                            document.querySelector('.popup-container').scrollTop = 0;
                        } else {
                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                        }
                    }
                }
            });
        });
    }
}

registerShowMoreAction();

// show more button single page end

//load more btn with animation start
// function registerLoadMoreAction() {
//     if (document.querySelector(".load-more")) {
//         document.querySelector(".load-more").addEventListener('click', function (e) {
//             document.querySelector(".load-more").classList.add("load-more--loading");
//             setTimeout(function (e) {
//                 document.querySelector(".load-more--loading").classList.remove("load-more--loading");
//             }, 3000);
//         })
//     }
// }

// registerLoadMoreAction()
//load more btn with animation end
///////////////////////////////////////////////////////////
//table single show all button start
function registerShowAllAction() {
    if (document.querySelector('.single__main__article__left-side__table-container')) {
        document.querySelectorAll('.show-all').forEach(item => {
            item.addEventListener('click', function () {
                let categoryColumn = this.closest('td').querySelector('a');
                if (categoryColumn) {
                    categoryColumn.style.overflow = 'unset';
                    categoryColumn.style.whiteSpace = 'unset';
                }
                this.style.display = "none"
                item.parentElement.parentElement.parentElement.querySelectorAll('.collapsable').forEach(item => {
                    item.classList.add('show')
                })
            })
        })
    }
}

registerShowAllAction();


//table single show all button end
/////////////////////////////////////////////////////////
//mockup shown items start
if (document.querySelector('.resource__other')) {
    let hiddenElements = 0;

    function mockupsController(itemsToShow) {
        document.querySelectorAll('.resource__card').forEach((item, index) => {
            if (index < itemsToShow) {
                document.getElementsByClassName('resource__card')[index].classList.remove('d-none')
            } else {
                hiddenElements++
            }
        })
        if (hiddenElements) {
            document.querySelector('.resource__other .number-of-hiddens').innerHTML = hiddenElements
        } else {
            document.querySelector('.resource__other').classList.add('d-none')
        }
    }

    let count = parseInt(document.querySelector("#catsnumbertodisplay").value);
    mockupsController(count) //elements to show
    document.querySelector('.resource__other').addEventListener('click', function () {
        document.querySelectorAll('.resource__card').forEach(item => {
            item.classList.remove('d-none')
            document.querySelector('.resource__other').classList.add('d-none')
        })
    })
}
//mockup shown items end
/////////////////////////////////////////////////
//sub menu index start
if (document.querySelector('.index__header')) {

    let usefulWidth = document.querySelector('.sub-menu').clientWidth - document.querySelector('.sub-menu').parentElement.clientWidth
    // let bodyWhiteSpace = document.querySelector('body').offsetWidth - document.querySelector('.container-fluid').offsetWidth
    // document.querySelector('.sub-menu').style.right = bodyWhiteSpace / 2 + 'px'

    window.addEventListener('resize', function () {
        let usefulWidth = document.querySelector('.sub-menu').clientWidth - document.querySelector('.sub-menu').parentElement.clientWidth
        // let bodyWhiteSpace = document.querySelector('body').offsetWidth - document.querySelector('.container-fluid').offsetWidth
    })
}
//sub menu index end
if (document.querySelector('.popup-container .close-btn')) {
    document.querySelector('.popup-container .close-btn').addEventListener('click', function () {
        document.querySelector('body').style.overflowY = 'auto'
        document.querySelector('.popup-container').classList.remove('show-popup')
        document.querySelector('.popup-container').classList.remove('popup-ready')
        document.querySelector('.popup').style.opacity = '0';
        document.querySelector('.popup-container').style.overflowY = 'hidden'
        document.body.style.marginRight = '0px'
        document.querySelector('.popup-loading-container').style.display = 'none';
        setTimeout(function () {
            document.querySelector('.popup-loading-container').style.display = 'flex';
            document.querySelector('.popup-container #popup').innerHTML = ''
        }, 300)
        // setTimeout(function () {
        // }, 500)
    })
}
if (document.querySelector('.header-scroll-btn')) {
    let counter = 0
    let slideLeft;
    document.querySelector('.right').addEventListener('click', function () {
        if (counter < document.querySelector('.header-light__navigation').clientWidth - document.querySelector('.header-light').clientWidth) {
            counter += 100;
            document.querySelector('.header-light').scroll(counter, 0)
        } else {
            document.querySelector('.header-light').scroll(document.querySelector('.header-light__navigation').clientWidth - document.querySelector('.header-light').clientWidth + counter, 0)
        }
    })
    document.querySelector('.left').addEventListener('click', function () {
        if (counter > 0) {
            counter -= 100;
            document.querySelector('.header-light').scroll(counter, 0)
        }
    })

    scrollBarTopHeaderLight();
}

window.addEventListener('resize', scrollBarTopHeaderLight)

function scrollBarTopHeaderLight() {
    if (document.querySelector('.index__header .header-dark .header-light')) {
        if (window.innerWidth - 200 < document.querySelector('.header-light__navigation').clientWidth) {
            document.querySelector('.index__header .header-dark .header-light').style.overflowX = "scroll"
            document.querySelector('.header-scroll-btn.right').style.display = "block"
            document.querySelector('.header-scroll-btn.left').style.display = "block"
        } else {
            // document.querySelector('.index__header .header-dark .header-light').style.overflowX = "hidden"
            document.querySelector('.header-scroll-btn.right').style.display = "none"
            document.querySelector('.header-scroll-btn.left').style.display = "none"
        }
    }
}


//////////////////////////////////
if (document.querySelector('#popup')) {
    if (window.innerWidth < 768 && document.querySelector('.next-btn')) {
        document.querySelector('.next-btn').classList.add('display-none')
        document.querySelector('.prev-btn').classList.add('display-none')
    } else {
        document.querySelector('.next-btn').classList.remove('display-none')
        document.querySelector('.prev-btn').classList.remove('display-none')
    }
    window.addEventListener('resize', function () {
        if (window.innerWidth < 768) {
            document.querySelector('.next-btn').classList.add('display-none')
            document.querySelector('.prev-btn').classList.add('display-none')
        } else {
            document.querySelector('.next-btn').classList.remove('display-none')
            document.querySelector('.prev-btn').classList.remove('display-none')
        }
    })

    function makePopupArticleImagesOnClick() {
        document.querySelectorAll('.article__img, .articleLink ').forEach(item => {
            item.addEventListener('click', function (e) {
                e.preventDefault();
                let widthBefore = document.body.clientWidth
                document.querySelector('body').style.overflowY = 'hidden'
                let widthAfter = document.body.clientWidth
                let compensateForScrollbar = widthAfter - widthBefore;
                document.body.style.marginRight = compensateForScrollbar + 'px'
                let article = this.closest('article')
                let currentId = article.dataset.currentid
                let prevID = article.dataset.previd
                let nextID = article.dataset.nextid
                let popupContainer = document.querySelector('.popup-container');
                let prevBtn = document.querySelector('.prev-btn')
                let nextBtn = document.querySelector('.next-btn')

                ajaxRequestPopupProducts('POST', `popup/popup${+currentId}.html`)

                // prevBtn.style.display = 'none'

                if (prevID != 0) {
                    prevBtn.style.display = 'block'
                } else {
                    prevBtn.style.display = 'none'
                }

                if (nextID != 0) {
                    nextBtn.style.display = 'block'
                } else {
                    nextBtn.style.display = 'none'
                }
                let popupNewContentTimeout;
                document.querySelector('.prev-btn').addEventListener('click', function () {

                    document.querySelector('.popup-container .popup').style.opacity = '0'
                    clearTimeout(popupNewContentTimeout)
                    popupNewContentTimeout = setTimeout(function () {
                        ajaxRequestPopupProducts('POST', `popup/popup${+prevID}.html`)

                        article = document.querySelector(`article[data-currentid="${+prevID}"]`)
                        currentId = article.dataset.currentid
                        prevID = article.dataset.previd
                        nextID = article.dataset.nextid

                        if (prevID != 0) {
                            prevBtn.style.display = 'block'
                        } else {
                            prevBtn.style.display = 'none'
                        }

                        if (nextID != 0) {
                            nextBtn.style.display = 'block'
                        } else {
                            nextBtn.style.display = 'none'
                        }
                    }, 300)

                })

                document.querySelector('.next-btn').addEventListener('click', function () {

                    document.querySelector('.popup-container .popup').style.opacity = '0';
                    clearTimeout(popupNewContentTimeout)
                    popupNewContentTimeout = setTimeout(function () {
                        ajaxRequestPopupProducts('POST', `popup/popup${+nextID}.html`)

                        article = document.querySelector(`article[data-currentid="${+nextID}"]`)
                        currentId = article.dataset.currentid
                        prevID = article.dataset.previd
                        nextID = article.dataset.nextid

                        if (prevID != 0) {
                            prevBtn.style.display = 'block'
                        } else {
                            prevBtn.style.display = 'none'
                        }

                        if (nextID != 0) {
                            nextBtn.style.display = 'block'
                        } else {
                            nextBtn.style.display = 'none'
                        }
                    }, 300)

                })
                document.querySelector('.popup-container').classList.add('show-popup')
            })
        })
    }

    makePopupArticleImagesOnClick();

    function ajaxRequestPopupProducts(method, url, async = true) {

        let popupContainer = document.querySelector('.popup-container');
        var xhttp = new XMLHttpRequest();
        xhttp.open(method, url, async);
        xhttp.send();

        xhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                popupContainer.querySelector("#popup").innerHTML += `<div id="the_content" style="display: none;">` + this.responseText + `</div>`;
                popupContainer.querySelector("#the_content").style.display = 'block';
                // Update LazyLoad action
                //registerLoadMoreAction();
                registerShowAllAction();
                /!*popupContainer.querySelector("#popup").style.display = 'block';*!/
                lazyLoadInstance.update();
                registerShowMoreAction();
                //makePopupArticleImagesOnClick();
                waitForElement("#popup .single__main__header__desc img", 3000).then(function () {
                    setTimeout(() => {
                        window.dispatchEvent(new Event('resize'));
                    }, 100);
                }).catch(() => {
                });

                if (document.querySelector('.popup .masonry-container')) {
                    let elem = document.querySelectorAll('.popup .masonry-container');
                    elem.forEach(function (item) {
                        msnry = new Masonry(item, {
                            itemSelector: '.masonry-item',
                            gutter: '.gutter-sizer',
                        });
                    })
                }
                if (document.querySelector('.popup .fake-table')) {
                    document.querySelector('.fake-table span:first-child').style.width = document.querySelector('.single-page__desc tbody tr td:first-child').clientWidth + 'px'
                    window.addEventListener('resize', function () {
                        document.querySelector('.fake-table span:first-child').style.width = document.querySelector('.single-page__desc tbody tr td:first-child').clientWidth + 'px'
                    })
                }
                if (document.querySelector('.popup .mockup-single__load-more')) {
                    document.querySelector('.popup .mockup-single__load-more').addEventListener('click', function () {
                        this.classList.add('spinning')

                        setTimeout(function () {
                            stopSpinner()
                        }, 2000)
                    })
                }
                if (document.querySelector('.popup .single-pages__img__show-more-btn')) {
                    document.querySelector('.popup .single-pages__img__show-more-btn').addEventListener('click', function () {
                        this.parentElement.classList.toggle('closed')
                    })
                }


                if (document.querySelector('.popup .single-page__desc p')) {
                    document.querySelector('.popup .single-page__desc p').style.height = document.querySelector('.popup .single-page__desc p').scrollHeight + 'px'

                    window.addEventListener('resize', function () {
                        document.querySelector('.popup .single-page__desc p').style.height = document.querySelector('.popup .single-page__desc p').scrollHeight + 'px'
                    })
                }

                if (document.querySelector('.popup .single-page__desc__show-more')) {
                    document.querySelector('.popup .single-page__desc__show-more').addEventListener('click', function () {
                        document.querySelector('.popup .single-page__desc p').classList.toggle('closed')
                        if (document.querySelector('.popup .single-page__desc__show-more').innerHTML === 'Show more') {
                            document.querySelector('.popup .single-page__desc__show-more').innerHTML = 'Show less'
                        } else {
                            document.querySelector('.popup .single-page__desc__show-more').innerHTML = 'Show more'
                        }
                    })
                }
                initSliderPopup()
                if (document.querySelector('.popup__masonry-container img[data-calc-ratio]')) {
                    document.querySelectorAll('.popup__masonry-container img[data-calc-ratio]').forEach(function (img, imgNumber,) {
                        if (img.dataset.calcRatio === 'true') {
                            img.parentElement.style.paddingTop = img.dataset.imgHeight * 100 / img.dataset.imgWidth + '%'
                        }
                        img.parentElement.classList.add('position-relative')
                        img.classList.add('img-with-placeholder')
                        if (imgNumber + 1 === document.querySelectorAll('.popup__masonry-container img[data-calc-ratio]').length) {
                            removeArticlesLoading()
                        }
                    })
                }
                let popupMasonry;

                if (document.querySelector('.popup__masonry-container')) {
                    let elem = document.querySelectorAll('.popup__masonry-container');
                    elem.forEach(function (item) {
                        popupMasonry = new Masonry(item, {
                            itemSelector: '.masonry-item',
                            gutter: '.gutter-sizer',
                        });
                    })

                }
                document.querySelector('.popup-container').style.overflowY = 'unset'
                document.querySelector('.popup-container').classList.add('popup-ready')
                document.querySelector('.popup-container .popup').style.opacity = '1'
            }
        };
    }
}

function waitForElement(querySelector, timeout) {
    return new Promise((resolve, reject) => {
        var timer = false;
        if (document.querySelectorAll(querySelector).length) return resolve();
        const observer = new MutationObserver(() => {
            if (document.querySelectorAll(querySelector).length) {
                observer.disconnect();
                if (timer !== false) clearTimeout(timer);
                return resolve();
            }
        });
        observer.observe(document.body, {
            childList: true,
            subtree: true
        });
        if (timeout) timer = setTimeout(() => {
            observer.disconnect();
            reject();
        }, timeout);
    });
}


// close popup onclick everywhere
window.addEventListener('click', function (e) {
    if (e.target.classList.contains('popup-container')) {
        document.querySelector('.popup-container .close-btn').click();
        document.querySelector('.popup-container').classList.remove('.popup-ready')
        document.querySelector('.popup-container').classList.remove('.show-popup')
    }
});

// if (/Android|webOS|iPhone|iPad|Mac|Macintosh|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
//     document.querySelectorAll('.single__main__article__left-side').forEach(item => {
//         if (item.clientHeight > window.innerHeight) {
//             document.querySelector('.single__main__header__desc').classList.add('remove-write-shadow')
//             document.querySelector('.single__main__article__left-side .show-more').style.display = "none"
//         }
//     })
// }

// document.querySelectorAll('.single__main__header__desc').forEach(item => {
//     if (item.scrollHeight <= document.querySelector('.single__main__article__right-side').scrollHeight) {
//         document.querySelector('.show-more').style.display = 'none'
//         document.querySelector('.single__main .article-container .single__main__article__left-side .single__main__header__desc').classList.add('remove-white-shadow')
//     }
// })
// window.addEventListener('resize', function () {
//     document.querySelectorAll('.single__main__header__desc').forEach(item => {
//         if (item.scrollHeight <= document.querySelector('.single__main__article__right-side').scrollHeight) {
//             document.querySelector('.show-more').style.display = 'none'
//             document.querySelector('.single__main .article-container .single__main__article__left-side .single__main__header__desc').classList.add('remove-white-shadow')
//         }
//     })
// })

let msnry;

function updateMasonry() {
    msnry.layout()
}

// (function () {
if (document.querySelector('.masonry-container')) {
    let elem = document.querySelectorAll('.masonry-container');
    elem.forEach(function (item) {
        msnry = new Masonry(item, {
            itemSelector: '.masonry-item',
            gutter: '.gutter-sizer',
        });
    })

}
// })();

if (document.querySelector('.categories-slider')) {
    let categoriesSlider = new Swiper(".categories-slider", {
        slidesPerView: 'auto',
        spaceBetween: 10,
        navigation: {
            nextEl: ".categories-slider__button-next",
            prevEl: ".categories-slider__button-prev",
        },
    });
}

if (document.querySelector('.header__categories-slider')) {
    let headerCategoriesSlider = new Swiper(".header__categories-slider", {
        slidesPerView: 'auto',
        spaceBetween: 32,
        navigation: {
            nextEl: ".header__categories-slider__button-next",
            prevEl: ".header__categories-slider__button-prev",
        },
    });
}


if (document.querySelector('.flyers__desc')) {
    document.querySelector('.flyers__desc p').style.maxHeight = document.querySelector('.flyers__desc p').scrollHeight + 'px'
    document.querySelector('.flyers__desc button').addEventListener('click', function () {
        this.parentElement.classList.toggle('closed')
        if (this.innerText === 'Show more') {
            this.innerText = 'Show less'
        } else {
            this.innerText = 'Show more'
        }
    })
}

if (document.querySelector('.commercial-checkbox-container')) {
    document.querySelector('.commercial-checkbox-container input').addEventListener('change', function () {
        document.querySelector('.commercial-use-badge').classList.toggle('active')
    })
}

if (document.querySelector('.commercial-use-badge button')) {
    document.querySelector('.commercial-use-badge').addEventListener('click', function () {
        document.querySelector('.commercial-checkbox-container').click()
    })
}

if (document.querySelector('.single-page__desc p')) {
    document.querySelector('.single-page__desc p').style.height = document.querySelector('.single-page__desc p').scrollHeight + 'px'

    document.querySelector('.single-page__desc__show-more').addEventListener('click', function () {
        document.querySelector('.single-page__desc p').classList.toggle('closed')
        if (document.querySelector('.single-page__desc__show-more').innerHTML === 'Show more') {
            document.querySelector('.single-page__desc__show-more').innerHTML = 'Show less'
        } else {
            document.querySelector('.single-page__desc__show-more').innerHTML = 'Show more'
        }
    })
}


//change pages with keyboard arrows
if (document.querySelector('.page-numbers')) {
    window.addEventListener('keydown', function (e) {
        if (e.key === 'ArrowRight') {
            alert('next page')
        }
    })
}

if (document.querySelector('.prev.page-numbers')) {
    window.addEventListener('keydown', function (e) {
        if (e.key === 'ArrowLeft') {
            alert('prev page')
        }
    })
}

if (document.querySelector('.mockup-single__copy-link')) {
    document.querySelectorAll('.mockup-single__copy-link').forEach(function (item) {
        item.addEventListener('click', function () {
            navigator.clipboard.writeText(document.querySelector('.mockup-single__copy-link').innerText)
            this.parentElement.querySelector('.mockup-single__copied-icon img:first-child').classList.add('d-none')
            this.parentElement.querySelector('.mockup-single__copied-icon img:last-child').classList.remove('d-none')
        })
    })

    document.querySelectorAll('.mockup-single__copied-icon').forEach(function (item) {
        item.addEventListener('click', function () {
            navigator.clipboard.writeText(document.querySelector('.mockup-single__copy-link').innerText)
            this.parentElement.querySelector('.mockup-single__copied-icon img:first-child').classList.add('d-none')
            this.parentElement.querySelector('.mockup-single__copied-icon img:last-child').classList.remove('d-none')
        })
    })
}

if (document.querySelector('.mockup-single__load-more')) {
    document.querySelector('.mockup-single__load-more').addEventListener('click', function () {
        this.classList.add('spinning')

        setTimeout(function () {
            stopSpinner()
        }, 2000)
    })
}

function stopSpinner() {
    document.querySelector('.mockup-single__load-more.spinning').classList.remove('spinning')
}

// document.querySelector('[data-currentid="1"] .articleLink').click()

//clear search field
if (document.querySelector('.clear-search-input')) {
    document.querySelector('.search-page__search-input input').addEventListener('keyup', function () {
        if (this.value === '') {
            document.querySelector('.clear-search-input').classList.add('d-none')
        } else {
            document.querySelector('.clear-search-input').classList.remove('d-none')
        }
    })

    document.querySelectorAll('.clear-search-input').forEach(function (item) {
        item.addEventListener('click', function () {
            this.parentElement.querySelector('input[type="search"]').value = "";
            document.querySelector('.clear-search-input').classList.add('d-none')
        })
    })
}

if (document.querySelector('.header__search-container')) {
    let headerNavigationWidth,
        headerLogoWidth,
        headerLogoMargin,
        indexHeader,
        indexHeaderPaddingLeft,
        indexHeaderPaddingRight,
        headerDark,
        headerDarkPaddingLeft,
        headerDarkPaddingRight,
        indexHeaderPaddingX,
        headerDarkPaddingX

    function setHeaderSearchWidth() {
        if (window.innerWidth >= 992) {
            headerNavigationWidth = document.querySelector('.header-light').clientWidth
            headerLogoWidth = document.querySelector('.header__logo-container').clientWidth
            headerLogoMargin = +window.getComputedStyle(document.querySelector('.header__logo-container'), null).getPropertyValue('margin-right').replace('px', '')
            indexHeader = document.querySelector('.index__header')
            indexHeaderPaddingLeft = +window.getComputedStyle(indexHeader, null).getPropertyValue('padding-left').replace('px', '')
            indexHeaderPaddingRight = +window.getComputedStyle(indexHeader, null).getPropertyValue('padding-right').replace('px', '')
            headerDark = document.querySelector('.header-dark')
            headerDarkPaddingLeft = +window.getComputedStyle(headerDark, null).getPropertyValue('padding-left').replace('px', '')
            headerDarkPaddingRight = +window.getComputedStyle(headerDark, null).getPropertyValue('padding-right').replace('px', '')
            indexHeaderPaddingX = +indexHeaderPaddingLeft + indexHeaderPaddingRight
            headerDarkPaddingX = +headerDarkPaddingLeft + headerDarkPaddingRight
            document.querySelector('.header__search-container').style.width = document.querySelector('body').offsetWidth - headerNavigationWidth - headerLogoWidth - headerLogoMargin - indexHeaderPaddingX - headerDarkPaddingX + 'px'
        }
    }

    setHeaderSearchWidth()

    window.addEventListener('resize', function () {
        setHeaderSearchWidth()
    })
}

if (document.querySelector('.single-pages__img__show-more-btn')) {
    document.querySelector('.single-pages__img__show-more-btn').addEventListener('click', function () {
        this.parentElement.classList.toggle('closed')

        if (this.innerText === "Show More") {
            this.innerText = "Show Less"
        } else {
            this.innerText = "Show More"
        }
    })
}

if (document.querySelector('.custom-select')) {
    document.addEventListener("DOMContentLoaded", function (e) {
        var els = document.querySelectorAll(".custom-select");
        els.forEach(function (select) {
            NiceSelect.bind(select);
        });
    });

    if (document.querySelector('.fonts-filters__sample-text-container select')) {
        document.querySelector('.fonts-filters__sample-text-container select').addEventListener('change', function () {
            document.querySelector('.fonts-filters__sample-text-container input').value = this.value
        })
    }
}

//font single page tabs
if (document.querySelector('.font-single__tabs')) {
    document.querySelectorAll('.font-single__tabs button').forEach(function (fontSingleTabBtn) {
        fontSingleTabBtn.addEventListener('click', function () {
            document.querySelector('.font-single__tabs button.active').classList.remove('active')
            this.classList.add('active')
            let targetTab = this.dataset.open
            document.querySelectorAll('.font-single__tabs-content').forEach(function (fontSingleTabContent) {
                fontSingleTabContent.classList.add('d-none')
                if (fontSingleTabContent.dataset.target === targetTab) {
                    fontSingleTabContent.classList.remove('d-none')
                }
            })
        })
    })
}

//font single show more and less related categories
if (document.querySelector('.font-single__show-and-hide__related-categories')) {
    document.querySelector('.related-categories').style.height = document.querySelector('.related-categories').scrollHeight + 5 + 'px'

    window.addEventListener('resize', function () {
        document.querySelector('.related-categories').style.height = document.querySelector('.related-categories').scrollHeight + 5 + 'px'
    })
    document.querySelector('.font-single__show-and-hide__related-categories').addEventListener('click', function () {
        document.querySelector('.related-categories').classList.toggle('closed')

        if (this.innerText === 'Show more') {
            this.innerText = 'Show less'
        } else {
            this.innerText = 'Show more'
        }
    })
}

function removeArticlesLoading() {
    let loadingContainer = document.querySelectorAll('.articles-container__loading-container')
    if (loadingContainer) {
        loadingContainer.forEach(function (item) {
            item.style.transitionDelay = '.5s'
            item.style.transition = '1s'
            item.classList.add('o-0')
        })
    }
}

function enterLazyLoadedImages(item) {
    // let articlesLoading = document.querySelector('.articles-container__loading-container')
}


if (document.querySelector('.search-page__categories')) {
    document.querySelectorAll('.search-page__categories button').forEach(function (button) {
        button.addEventListener('click', function () {
            if (button.classList.contains('active')) return
            document.querySelector('.search-page__categories button.active').classList.remove('active')
            this.classList.add('active')
            let thisButton = this
            document.querySelectorAll('div[data-target]').forEach(function (searchDiv) {
                if (searchDiv.dataset.target === thisButton.dataset.open) {
                    searchDiv.classList.remove('d-none')
                } else {
                    searchDiv.classList.add('d-none')
                }
                lazyLoadInstance.update()
                msnry.layout()
                removeArticlesLoading()
            })
        })
    })
}

if (document.querySelector('[data-scroll-to]')) {
    document.querySelectorAll('[data-scroll-to]').forEach(function (button) {
        button.addEventListener('click', function () {
            window.scrollTo(0, document.querySelector('#' + this.dataset.scrollTo).getBoundingClientRect().top - document.body.getBoundingClientRect().top)
        })
    })
}

if (document.querySelector('.single-page__slider')) {
    let swiper = new Swiper(".single-page__slider", {
        speed: 500,
        loop: 'true',
        effect: 'fade',
        navigation: {
            nextEl: ".single-page__slider-button-next",
            prevEl: ".single-page__slider-button-prev",
        },
        pagination: {
            el: ".single-page__pagination",
            clickable: true,
        },
    });
}

if (document.querySelector('.single-page__slider')) {
    let navigations = document.querySelectorAll('.single-page__slider .swiper-pagination-bullet')
    let iframes = [];
    document.querySelectorAll('.single-page__slider .swiper-slide').forEach(function (iframeContainer) {
        if (iframeContainer.querySelector('iframe')) {
            navigations[iframeContainer.dataset.swiperSlideIndex].innerHTML = "<img src='assets/images/youtube-icon.svg'>"
            navigations[iframeContainer.dataset.swiperSlideIndex].classList.add('have-youtube-icon')
        }
    })
}

function initSliderPopup() {
    if (document.querySelector('.popup__slider')) {
        let swiper = new Swiper(".popup__slider", {
            speed: 500,
            effect: 'fade',
            loop: 'true',
            navigation: {
                nextEl: ".popup__slider .single-page__slider-button-next",
                prevEl: ".popup__slider .single-page__slider-button-prev",
            },
            pagination: {
                el: ".single-page__pagination",
                clickable: true,
            },
        });
    }

    if (document.querySelector('.popup__slider')) {
        let navigations = document.querySelectorAll('.popup__slider .swiper-pagination-bullet')
        let iframes = [];
        document.querySelectorAll('.popup__slider .swiper-slide').forEach(function (iframeContainer) {
            if (iframeContainer.querySelector('iframe')) {
                navigations[iframeContainer.dataset.swiperSlideIndex].innerHTML = "<img src='assets/images/youtube-icon.svg'>"
                navigations[iframeContainer.dataset.swiperSlideIndex].classList.add('have-youtube-icon')
            }
        })
    }
}

if (document.querySelector('.variant')) {
    document.querySelector('.show-variants-checkbox').addEventListener('change', function () {
        document.querySelector('.variant').parentElement.parentElement.classList.toggle('font-article__height')
        document.querySelectorAll('.variant').forEach(function (img) {
            img.classList.toggle('d-none')
        })
    })
}

if (document.querySelector('.sub-menu')) {
    document.querySelector('.sub-menu').style.display = 'block'
}

if (document.querySelector('.js-header-mobile__menu')) {
    document.querySelector('.js-header-mobile__menu').style.display = 'block'
}

if (document.querySelector('.cards-section__header')) {
    document.querySelectorAll('.cards-section__header button').forEach(function (btn) {
        btn.addEventListener('click', function () {
            document.querySelectorAll('.cards-section__header button').forEach(function (item) {
                item.classList.remove('active')
            })
            this.classList.add('active')
            document.querySelectorAll(`[data-open]`).forEach(function (item) {
                item.classList.add('d-none')
            })
            document.querySelector(`[data-open="${this.dataset.target}"]`).classList.remove('d-none')
            msnry.layout()
            removeArticlesLoading()
        })
    })
}

if (document.querySelector('.have-button-active-effect')) {
    function setButtonActiveBorder(clickedBtn = document.querySelector('.have-button-active-effect').parentElement.querySelector('.active')) {
        let width = document.querySelector('.have-button-active-effect').parentElement.querySelector('.active').clientWidth
        let left = clickedBtn.getBoundingClientRect().left - document.querySelector('.have-button-active-effect').parentElement.getBoundingClientRect().left
        document.querySelector('.have-button-active-effect').style.width = width + 'px'
        document.querySelector('.have-button-active-effect').style.left = left + 'px'
    }

    setButtonActiveBorder()
}


if (document.querySelector('.have-button-active-effect')) {
    document.querySelector('.have-button-active-effect').parentElement.querySelectorAll('button').forEach(function (btn) {
        btn.addEventListener('click', function () {
            document.querySelectorAll('.author-products__heading button').forEach(function (removeAvtive) {
                removeAvtive.classList.remove('active')
            })
            btn.classList.add('active')
            setButtonActiveBorder(this)
        })
    })
}


// run this function after aside search ul changed with ajax

if (document.querySelector('.aside__search-field button')) {
    let firstHeight;

    function setSearchAsideHeight() {
        document.querySelector('.aside__search-field ul').setAttribute('data-first-height', `${document.querySelector('.aside__search-field ul').scrollHeight + 'px'}`);
        document.querySelector('.aside__search-field ul').style.maxHeight = document.querySelector('.aside__search-field ul').scrollHeight + 'px';
        firstHeight = document.querySelector('.aside__search-field ul').scrollHeight + 'px';
    }

    setSearchAsideHeight()
    window.addEventListener('resize', function () {
        setSearchAsideHeight()
    })

    let doIt, height, i, thisLength;
    document.querySelector('.aside__search-field button').addEventListener('click', function () {
        doIt = 'true';
        i = 0;
        thisLength = this.parentElement.querySelectorAll('ul li[data-display-toggle="true"]').length
        this.parentElement.querySelectorAll('ul li[data-display-toggle="true"]').forEach(function (item) {
            if (item.classList.contains('d-none')) {
                item.classList.remove('d-none')
                height = document.querySelector('.aside__search-field ul').scrollHeight + 'px';
                if (thisLength === ++i) {
                    document.querySelector('.aside__search-field ul').style.maxHeight = height;
                    doIt = 'false';
                }
            } else {
                if (doIt === 'true') {
                    document.querySelector('.aside__search-field ul').style.maxHeight = firstHeight;
                    doIt = 'false';
                }
                setTimeout(function () {
                    item.classList.add('d-none')
                }, 200)
            }
        })
    })
}

// if (document.querySelector('.aside__search-field button')) {
//     document.querySelector('.aside__search-field button').addEventListener('click', function () {
//         document.querySelectorAll('.aside__search-field li.d-none').forEach(function (li) {
//             li.classList.remove('d-none')
//         })
//     })
// }


if (document.querySelector('.aside__search-field')) {
    setSearchAsideHeight()
}


//set width of fake table in single pages
if (document.querySelector('.fake-table')) {
    document.querySelector('.fake-table span:first-child').style.width = document.querySelector('.single-page__desc tbody tr td:first-child').clientWidth + 'px'
    window.addEventListener('resize', function () {
        document.querySelector('.fake-table span:first-child').style.width = document.querySelector('.single-page__desc tbody tr td:first-child').clientWidth + 'px'
    })
}

function singlePageDescHeight() {
    if (document.querySelector('.single-page__img-container .swiper-wrapper')) {
        document.querySelector('.single-page__desc').style.minHeight = document.querySelector('.single-page__img-container .swiper-wrapper').clientHeight + 'px';

        window.addEventListener('resize', function () {
            document.querySelector('.single-page__desc').style.minHeight = document.querySelector('.single-page__img-container .swiper-wrapper').clientHeight + 'px';
        })
    }

    if (document.querySelector('.brand-font-single__img-container')) {
        document.querySelector('.single-page__desc').style.minHeight = document.querySelector('.brand-font-single__img-container').clientHeight + 'px';

        window.addEventListener('resize', function () {
            document.querySelector('.single-page__desc').style.minHeight = document.querySelector('.brand-font-single__img-container').clientHeight + 'px';
        })
    }
}

singlePageDescHeight()

if (document.querySelector('.brand-font-single__img-container')) {
    if ((67 / 100) * window.innerHeight > 630) {
        document.querySelector('.brand-font-single__img-container').classList.add('no-min-height');
        singlePageDescHeight()
    } else {
        document.querySelector('.brand-font-single__img-container').classList.remove('no-min-height');
    }

    window.addEventListener('resize', function () {
        if ((67 / 100) * window.innerHeight > 630) {
            singlePageDescHeight()
            document.querySelector('.brand-font-single__img-container').classList.add('no-min-height');
        } else {
            document.querySelector('.brand-font-single__img-container').classList.remove('no-min-height');
        }
    })
}

if (document.querySelector('.font-single__tabs__carousel')) {
    const fontSingleTabsCarousel = new Swiper('.font-single__tabs__carousel', {
        slidesPerView: 'auto',
        slideToClickedSlide: 'auto',
        navigation: {
            nextEl: ".font-single__tabs-slider__button-next",
            prevEl: ".font-single__tabs-slider__button-prev",
        },
    });
}

if (document.querySelector('.font-single-main .fonts-filters__sample-text-container')) {

    let decWidth = document.querySelector('.fonts-filters-container').nextElementSibling.clientWidth;
    document.querySelector('.fonts-filters__sample-text-container').style.maxWidth = `calc(100% - ${decWidth}px)`

    window.addEventListener('resize', function () {
            document.querySelector('.fonts-filters__sample-text-container').style.maxWidth = `calc(100% - ${decWidth}px)`
        }
    )
}