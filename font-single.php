<?php include 'header2.php'; ?>
    <div class="main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>

        <main class="font-single-main">
            <h1 class="main__heading font-single__main-heading">Lemon Milk Font<sub><a href="">by Typodermic
                        Fonts</a></sub></h1>
            <div class="categories-slider swiper">
                <div class="swiper-wrapper">
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                </div>
                <div class="categories-slider__button-next swiper-button-next"></div>
                <div class="categories-slider__button-prev swiper-button-prev"></div>
            </div>
            <div class="d-flex align-items-stretch flex-column flex-sm-row">
                <aside class="aside-search-container d-none d-sm-block">
                    <div>
                        <div class="aside__heading">
                            <img data-src="assets/images/4-squares.svg" class="js-lazy">
                            <span>Mockups</span>
                        </div>
                        <div class="aside__search-field">
                            <label class="d-flex align-items-center">
                                <img data-src="assets/images/search-icon.svg" class="js-lazy">
                                <input type="search" placeholder="Search">
                            </label>
                            <ul>
                                <li><a href="">Apple Device</a></li>
                                <li><a href="">Book</a></li>
                                <li><a href="">Bottle</a></li>
                                <li><a href="">Box</a></li>
                                <li><a href="">Branding</a></li>
                                <li><a href="">Brochure</a></li>
                                <li><a href="">Business Card</a></li>
                                <li><a href="">Device</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                            </ul>
                            <button href="#">+ Show more</button>
                        </div>
                    </div>
                </aside>
                <section class="articles index__articles">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <div class="d-flex align-items-center fonts-filters-container">
                            <div class="fonts-filters__sample-text-container">
                                <input type="text" placeholder="Type your text">
                                <select class="custom-select">
                                    <option value="text 1">text 1</option>
                                    <option value="text 2">text 2</option>
                                    <option value="text 3">text 3</option>
                                    <option value="text 4">text 4</option>
                                </select>
                            </div>
                            <div class="d-flex align-items-center">
                                <img src="assets/images/font-size.svg" class="fonts-filters__font-size-icon">
                                <input type="range" min="1" max="100" value="50" step="1"
                                       class="fonts-filters__range-slider">
                            </div>
                            <button class="d-flex align-items-center fonts-filters__reset-btn">
                                <img src="assets/images/reset.svg" class="fonts-filters__info-icon">
                                Reset
                            </button>
                        </div>
                        <div class="d-flex align-items-center pt-28">
                            <button class="d-flex align-items-center bg-transparent"
                                    data-scroll-to="commentsSection">
                                <span class="comments-number me-1">36</span>
                                <img src="assets/images/comment.svg" alt="">
                            </button>
                            <div class="position-relative share-btns-icon d-flex align-items-center ms-4">
                                <img class="js-lazy loaded" data-ll-status="loaded"
                                     src="assets/images/mobile-size-share-icon.svg">
                                <div class="share-btns-container mobile-size-share-btns-container">
                                    <div class="d-flex align-items-center justify-content-between">
                                        <button>
                                            <img class="js-lazy loaded" data-ll-status="loaded"
                                                 src="assets/images/facebook-share-btn.svg">
                                        </button>
                                        <button>
                                            <img class="js-lazy loaded" data-ll-status="loaded"
                                                 src="assets/images/twitter-share-btn.svg">
                                        </button>
                                        <button>
                                            <img class="js-lazy loaded" data-ll-status="loaded"
                                                 src="assets/images/pinterest-share-btn.svg">
                                        </button>
                                        <button>
                                            <img class="js-lazy loaded" data-ll-status="loaded"
                                                 src="assets/images/email-share-btn.svg">
                                        </button>
                                    </div>
                                    <div>
                                        <div class="mockup-single__copy-link">
                                            https://resourceboy.com/photo
                                        </div>
                                        <div class="mockup-single__copied-icon">
                                            <img class="js-lazy loaded" data-ll-status="loaded"
                                                 src="assets/images/copied.svg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button class="d-flex align-items-center ms-4 bg-transparent"><img
                                        src="assets/images/bookmark.svg" alt=""></button>
                        </div>
                    </div>
                    <div class="d-flex align-items-start flex-column flex-lg-row justify-content-between font-single__main-product">
                        <div class="w-100">
                            <img data-src="assets/images/lemon-milk.svg" class="js-lazy img-fluid">
                            <div class="font-single__tabs">
                                <div class="font-single__tabs__carousel swiper">
                                    <div class="swiper-wrapper">
                                        <button class="swiper-slide active" data-open="first">Description</button>
                                        <button class="swiper-slide" data-open="second">Note of the author</button>
                                        <button class="swiper-slide" data-open="third">Character Map</button>
                                    </div>
                                    <div class="font-single__tabs-slider__button-next swiper-button-next"></div>
                                    <div class="font-single__tabs-slider__button-prev swiper-button-prev"></div>
                                </div>
                                <div>
                                    <div data-target="first" class="font-single__tabs-content">
                                        If you are a football fan and crave to design sports
                                        graphic
                                        content, we have a special offer for you such as Forza
                                        Juve
                                        that comes with accurate curves, thus conveying a
                                        feeling of
                                        illusion, dynamic, innovation, or eerie. This all-caps
                                        maze
                                        font can be your practical assistant in creating
                                        high-end
                                        artwork like soccer posters, logos, brochures, or
                                        <a href="">T-shirts</a>.
                                    </div>
                                    <div data-target="second" class="d-none font-single__tabs-content">
                                        <blockquote>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci
                                            consequuntur
                                            ducimus eos est maiores nesciunt pariatur ratione repellat voluptas
                                            voluptatum!
                                            A aliquam cupiditate dolor iste laborum maxime perspiciatis quisquam
                                            sapiente.
                                        </blockquote>
                                    </div>
                                    <div data-target="third" class="d-none font-single__tabs-content">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. In modi nesciunt
                                        omnis tempora veritatis? Aliquid cupiditate dolores doloribus eos, ipsa magnam
                                        maiores, modi nisi odio porro repellendus totam vel voluptates.
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="single-page__desc">
                            <div>
                                <div class="font-single__license">
                                    <span>License:</span>
                                    <span>Personal and commercial use</span>
                                </div>
                                <div class="d-flex flex-column">
                                    <button class="go-to-free-dl">
                                        Free Download ↓
                                    </button>
                                    <button class="go-to-free-dl-cm position-relative"><img
                                                data-src="assets/images/link-icon.svg"
                                                class="go-to-free-dl__cm-icon js-lazy">Go to
                                        Free Download →
                                    </button>
                                </div>
                                <div class="related-categories closed">
                                    <span class="related-categories__heading">Related Categories</span>
                                    <div class="d-flex flex-wrap">
                                        <a href="">Geometric</a>
                                        <a href="">Poster</a>
                                        <a href="">Art Deco</a>
                                        <a href="">Modern</a>
                                        <a href="">Logo</a>
                                        <a href="">Summer</a>
                                        <a href="">Tropical</a>
                                        <a href="">Geometric</a>
                                        <a href="">Geometric</a>
                                        <a href="">Geometric</a>
                                        <a href="">Geometric</a>
                                        <a href="">Geometric</a>
                                    </div>
                                </div>
                                <button class="font-single__show-and-hide__related-categories">Show more</button>
                            </div>
                        </div>
                    </div>
                    <div class="articles-container articles-container-1 font-articles-container">
                        <span class="similar-fonts__heading">Similar Fonts</span>
                        <?php include 'articles-loading.php' ?>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/panchang.svg" class="js-lazy" data-img-width="344"
                                         data-img-height="106" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">2 styles</span>
                                        <span>Personal and commercial use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira.svg" class="js-lazy" data-img-width="119"
                                         data-img-height="70" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/mystic-crystal.svg" class="js-lazy"
                                         data-img-width="494" data-img-height="107" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">12 Styles</span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue.svg" class="js-lazy" data-img-width="547"
                                         data-img-height="132" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue2.svg" class="js-lazy" data-img-width="505"
                                         data-img-height="128" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article
                                class="font-article d-flex align-items-center justify-content-between font-article__height  ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira2.svg" class="js-lazy" data-img-width="238"
                                         data-img-height="116" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                    </div>
                    <div class="d-flex justify-content-center">
                        <button class="mockup-single__load-more"><span>Load More</span></button>
                    </div>
                </section>
            </div>
            <section class="comments-section container" id="commentsSection">
                <span class="comments-section__header">7 comments </span>
                <div class="comments-container">
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="d-flex align-items-center">
                            <div class="comments__avatar-container">
                                <img data-src="assets/images/avatar.png" class="js-lazy">
                            </div>
                            <span>Steve Giddings</span>
                            <span class="Rb-stuff-badge">RB Stuff</span>
                        </div>
                        <div class="mt-2">
                            <p>
                                Thanks for this Brian! I am really struggling to get links without going the paid route
                                (which I am not!) and will try this approach for sure. I’m just concerned that on a new
                                site, the article page will not feature in search and not get the traction to draw in
                                the links.

                            </p>
                            <button class="comment__reply-button">Reply</button>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container">
                                    <img data-src="assets/images/avatar.png" class="js-lazy">
                                </div>
                                <span class="comment-username">Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                        <div class="comment__reply">
                            <div class="d-flex align-items-center">
                                <div class="comments__avatar-container no-pic-avatar">
                                    <img data-src="assets/images/no-pic-avatar.svg" class="js-lazy">
                                </div>
                                <span>Steve Giddings</span>
                                <span class="Rb-stuff-badge">RB Stuff</span>
                            </div>
                            <div class="mt-2">
                                <p>
                                    Thanks for this Brian! I am really struggling to get links without going the paid
                                    route
                                    (which I am not!) and will try this approach for sure. I’m just concerned that on a
                                    new
                                    site, the article page will not feature in search and not get the traction to draw
                                    in
                                    the links.

                                </p>
                                <button class="comment__reply-button">Reply <img data-src="assets/images/reply-icon.svg"
                                                                                 class="js-lazy"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <span class="comment-section__title">Leave a Comment</span>
                    <span class="comment-section__desc">Your email address will not be published. Required fields are marked *</span>
                    <form class="leave-a-comment-form row">
                        <label>
                            <textarea name="comment" id="" cols="30" rows="10" placeholder="Your comment *"
                                      class="col-12"></textarea>
                        </label>
                        <label>
                            <input type="text" placeholder="Your name *" name="name" class="col-12">
                        </label>
                        <label>
                            <input type="text" placeholder="Your email address *" name="email" class="col-12">
                        </label>
                        <div>
                            <button class="">Submit your comment →</button>
                        </div>
                    </form>
                </div>
            </section>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>