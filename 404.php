<?php include 'header2.php'; ?>
    <div class="container-fluid main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Home
        </a>

        <main class="not-found-main">
            <div class="d-flex align-items-center justify-content-center not-found__heading-container">
                <div class="position-relative d-inline">
                    <span class="not-found-heading">Page not found</span>
                    <span class="not-found-desc">Search for content using the search bar or click one of the options below.</span>
                    <img data-src="assets/images/not-found-icon.svg" class="not-found-icon js-lazy">
                </div>
            </div>
            <section class="author-products-container articles">
                <span>Resource Boy Exclusives</span>
                <div class="articles-container masonry-container articles-container-7">
                    <?php include 'articles-loading.php' ?>
                    <div class="gutter-sizer"></div>
                    <article class="masonry-item pin">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-6.png" data-img-width="174"
                                     data-img-height="256" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cum ex
                                    facere laborum minus molestiae omnis quia reiciendis rem rerum.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item pin">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque dicta harum id
                                    iste nesciunt perspiciatis quae repellendus ullam ut voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png" data-img-width="174"
                                     data-img-height="223" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita laborum nihil
                                    qui quis, reiciendis sapiente tempora. Aliquam dicta earum sit.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus, animi
                                    beatae dolores illo in molestiae quaerat velit voluptatum. A, laudantium?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus architecto
                                    autem blanditiis commodi consequatur illum iste optio sit temporibus tenetur.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque ducimus ipsa
                                    laboriosam laborum neque possimus quasi saepe vel voluptas voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, doloribus
                                    error facere itaque molestias quasi qui quia reiciendis temporibus ut?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, distinctio dolore
                                    eveniet itaque labore nesciunt perspiciatis. Iusto natus nobis quasi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto
                                    aspernatur beatae ea eaque neque non officiis praesentium repudiandae voluptate!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab commodi dolorem,
                                    doloremque facilis modi natus neque quia quis quo repellat!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda deleniti
                                    dolorem ea esse fuga magnam maiores modi, nam provident quibusdam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolores enim
                                    est et exercitationem fugiat nam neque obcaecati saepe totam!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png" data-img-width="174"
                                     data-img-height="223" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi est
                                    expedita fugiat ratione recusandae repellendus sed voluptates. Omnis, optio!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png" data-img-width="174"
                                     data-img-height="223" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, quae, quis?
                                    Debitis, dignissimos dolor eligendi maxime mollitia nostrum quaerat qui.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, quaerat, ullam?
                                    Architecto dolore ipsam molestias neque nulla quibusdam quod sunt!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab aliquid consectetur
                                    consequatur eos nostrum provident reprehenderit, repudiandae sint ullam
                                    voluptate.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam doloribus
                                    dolorum explicabo libero molestiae mollitia porro ratione repudiandae saepe?
                                    Magnam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png" data-img-width="174"
                                     data-img-height="223" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid
                                    aspernatur earum perferendis perspiciatis. Facilis hic ipsum iste officiis
                                    ratione!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet animi incidunt
                                    ipsum molestias nihil quae quam quia, soluta sunt voluptatum.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti natus quo
                                    similique tempore! A asperiores distinctio quaerat repellendus rerum sunt.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore maiores,
                                    ullam. Commodi dolorum est non quae reprehenderit, vero voluptatem. Maiores!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/Rb-img-7.png" data-img-width="174"
                                     data-img-height="223" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores autem
                                    doloribus hic incidunt nam nisi obcaecati praesentium quos veritatis voluptas!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid amet fuga
                                    laudantium odio possimus quam quis unde veniam. Accusamus, laboriosam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, eaque fugiat
                                    harum libero neque nihil reiciendis sed sunt! Delectus, sit.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos eaque
                                    error est ipsa laboriosam molestias nam quisquam reiciendis similique veniam?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-8.png" data-img-width="174"
                                     data-img-height="174" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur cupiditate
                                    eius esse expedita harum itaque magni minima, nihil quae quod!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias architecto atque
                                    aut dicta dolore dolorum ex optio provident quae similique.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem beatae, delectus
                                    dolorem esse facilis molestias obcaecati quam qui quia tenetur.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur cum
                                    explicabo fugiat libero veritatis. Adipisci aperiam atque aut dolores molestias.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis
                                    deleniti, doloremque et fuga impedit iure numquam officia quos voluptates.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio excepturi
                                    hic ipsum laboriosam laudantium nesciunt nobis officiis. Error, iste, nisi.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci ducimus ea
                                    illo ipsum maiores non quaerat quisquam vel vero voluptatibus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dicta dolore
                                    excepturi ipsa, minima nam nemo officiis perspiciatis quos unde!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium commodi
                                    eligendi est facere impedit itaque maiores numquam recusandae repudiandae ut.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque, dolorum
                                    impedit in laudantium molestiae neque similique totam? Amet illo, minima!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo ipsam mollitia
                                    nemo nulla odit quidem ratione reprehenderit rerum sapiente vero.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur dolorem
                                    minus mollitia nisi non nulla odit, officia sint veniam voluptates!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda atque
                                    cumque facere inventore neque odit pariatur perspiciatis unde, voluptatibus!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium adipisci
                                    aperiam architecto deleniti facere incidunt molestias quaerat quam repellat
                                    ullam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi consectetur
                                    dolore id illum pariatur quae quas reprehenderit sapiente sunt voluptate.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium deserunt
                                    dolorem earum esse impedit nihil perspiciatis praesentium, ratione recusandae
                                    repellendus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ducimus nemo
                                    officiis, possimus quam ratione recusandae voluptatum. Cumque, delectus,
                                    necessitatibus!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci animi dolores
                                    dolorum ipsam minus nostrum optio sapiente sed tenetur! Magnam!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium cupiditate
                                    error facere fugiat molestiae non ratione rerum? Ad, earum repudiandae.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias consequatur
                                    culpa deleniti dolor modi necessitatibus nesciunt quia quidem suscipit.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus atque
                                    consequuntur debitis deserunt distinctio harum nihil placeat praesentium
                                    quisquam vero.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti ex, iure
                                    magnam maiores officia omnis ratione. Alias cupiditate debitis delectus.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam
                                    cupiditate ea et explicabo mollitia nisi, omnis placeat quasi sequi?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusamus ad alias
                                    aut corporis maxime nemo non odio qui ullam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto autem
                                    cupiditate eos, error libero nesciunt nihil officia quos voluptatem voluptatum?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis cupiditate
                                    dignissimos doloribus eveniet illum iusto, magni perspiciatis quidem voluptas.
                                    Unde.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad consequatur
                                    doloremque eaque enim illum laboriosam minima omnis provident reiciendis veniam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae cupiditate
                                    deleniti ipsum natus nesciunt odio quis repellat sunt. Amet, quam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus esse incidunt
                                    officia quas repellat sapiente! Ab labore modi nemo voluptatum!
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa nesciunt rem
                                    soluta. A accusamus accusantium cum eos saepe sed similique.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores assumenda
                                    delectus dolore dolores earum ex in odit ratione recusandae voluptatem?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero maiores
                                    nesciunt officiis possimus quis recusandae rerum vel. Cum debitis, ipsum?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus enim ipsum
                                    iure nemo repellendus reprehenderit veritatis voluptas. Labore, sequi, veniam.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-3.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores illum ipsa
                                    voluptatibus! Accusamus corporis, deserunt distinctio eveniet in repellat
                                    veritatis?
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                    <article class="masonry-item">
                        <div class="position-relative">
                            <a href="" class="article__img">
                                <img class="js-lazy" alt="" data-src="assets/images/RB-img-1.png" data-img-width="174"
                                     data-img-height="116" data-calc-ratio="true">
                            </a>
                            <div class="versions-number">
                                <img data-src="assets/images/slides-icon.svg" class="js-lazy">
                                <span>12</span>
                                <div class="versions__desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis cum eaque
                                    eveniet illo iure laudantium maiores nam pariatur quo veritatis.
                                </div>
                            </div>
                        </div>
                        <div class="article__caption">
                            <a href="">
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <h2 class="article__titre">Mini Glass Water Bottle Mockup</h2>
                            </a>
                        </div>
                    </article>
                </div>
            </section>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>