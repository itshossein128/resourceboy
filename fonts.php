<?php include 'header2.php'; ?>
    <div class="main-content-container">
        <a href="" class="back-location">
            <span class="back-location__arrow">←</span> Mockups
        </a>

        <main class="fonts-main">
            <h1 class="main__heading">Sans Serif Fonts</h1>
            <div class="categories-slider swiper">
                <div class="swiper-wrapper">
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                    <a class="swiper-slide" href="">Brochure Mockups</a>
                </div>
                <div class="categories-slider__button-next swiper-button-next"></div>
                <div class="categories-slider__button-prev swiper-button-prev"></div>
            </div>
            <div class="d-flex align-items-stretch flex-column flex-sm-row">
                <aside class="aside-search-container d-none d-sm-block">
                    <div>
                        <div class="aside__heading">
                            <img data-src="assets/images/4-squares.svg" class="js-lazy">
                            <span>Mockups</span>
                        </div>
                        <div class="aside__search-field">
                            <label class="d-flex align-items-center">
                                <img data-src="assets/images/search-icon.svg" class="js-lazy">
                                <input type="search" placeholder="Search">
                            </label>
                            <ul>
                                <li><a href="">Apple Device</a></li>
                                <li><a href="">Book</a></li>
                                <li><a href="">Bottle</a></li>
                                <li><a href="">Box</a></li>
                                <li><a href="">Branding</a></li>
                                <li><a href="">Brochure</a></li>
                                <li><a href="">Business Card</a></li>
                                <li><a href="">Device</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                                <li class="d-none" data-display-toggle="true"><a href="">Food / Beverage</a></li>
                            </ul>
                            <button href="#">+ Show more</button>
                        </div>
                    </div>
                </aside>
                <section class="articles index__articles">
                        <div class="d-flex align-items-center justify-content-start">
                            <div>
                                <div class="cards-section__header">
                                    <button class="popular-btn active" data-target="popular">Popular</button>
                                    <button class="newest-btn" data-target="newest">Newest</button>
                                </div>
                            </div>
                        </div>
                    <div class="d-flex align-items-center fonts-filters-container mb-2">
                        <select class="custom-select fonts-filters__number-of-products">
                            <option value="50" selected>50</option>
                            <option value="1">10</option>
                            <option value="2">20</option>
                            <option value="3">30</option>
                        </select>
                        <div class="fonts-filters__sample-text-container">
                            <input type="text" placeholder="Type your text">
                            <select class="custom-select">
                                <option value="text 1">text 1</option>
                                <option value="text 2">text 2</option>
                                <option value="text 3">text 3</option>
                                <option value="text 4">text 4</option>
                            </select>
                        </div>
                        <div class="d-flex align-items-center">
                            <img src="assets/images/font-size.svg" class="fonts-filters__font-size-icon">
                            <input type="range" min="1" max="100" value="50" step="1"
                                   class="fonts-filters__range-slider">
                        </div>
                        <label class="d-flex align-items-center line-height-0 fs-13">
                            <input type="checkbox" class="show-variants-checkbox">
                            Show variants
                            <img src="assets/images/Info.svg" class="show-variants-icon">
                        </label>
                        <label class="commercial-checkbox-container my-0 line-height-0">
                            <input type="checkbox">
                            Only commercial use license
                        </label>
                        <button class="d-flex align-items-center fonts-filters__reset-btn line-height-0">
                            <img src="assets/images/reset.svg" class="fonts-filters__info-icon">
                            Reset
                        </button>
                    </div>
                    <div class="articles-container articles-container-1 font-articles-container" data-open="popular">
                        <?php include 'articles-loading.php' ?>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                                <div class="article__img variant d-none">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                                <div class="article__img variant d-none">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/panchang.svg" class="js-lazy" data-img-width="344"
                                         data-img-height="106" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">2 styles</span>
                                        <span>Personal and commercial use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira.svg" class="js-lazy" data-img-width="119"
                                         data-img-height="70" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/mystic-crystal.svg" class="js-lazy"
                                         data-img-width="494" data-img-height="107" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">12 Styles</span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue.svg" class="js-lazy" data-img-width="547"
                                         data-img-height="132" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue2.svg" class="js-lazy" data-img-width="505"
                                         data-img-height="128" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira2.svg" class="js-lazy" data-img-width="238"
                                         data-img-height="116" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                    </div>
                    <div class="articles-container articles-container-1 font-articles-container d-none" data-open="newest">
                        <?php include 'articles-loading.php' ?>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/panchang.svg" class="js-lazy" data-img-width="344"
                                         data-img-height="106" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">2 styles</span>
                                        <span>Personal and commercial use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  pin">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                                <div class="article__img variant d-none">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                                <div class="article__img variant d-none">
                                    <img data-src="assets/images/lemon-milk.svg" class="js-lazy" data-img-width="471"
                                         data-img-height="100" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/mystic-crystal.svg" class="js-lazy"
                                         data-img-width="494" data-img-height="107" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles">12 Styles</span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira.svg" class="js-lazy" data-img-width="119"
                                         data-img-height="70" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue.svg" class="js-lazy" data-img-width="547"
                                         data-img-height="132" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/bebas-neue2.svg" class="js-lazy" data-img-width="505"
                                         data-img-height="128" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                        <article class="font-article d-flex align-items-center justify-content-between font-article__height  ">
                            <div>
                                <img data-src="assets/images/pin.svg" class="article__pin js-lazy">
                                <div class="article__img">
                                    <img data-src="assets/images/akira2.svg" class="js-lazy" data-img-width="238"
                                         data-img-height="116" data-calc-ratio="true">
                                </div>
                            </div>
                            <div class="d-flex flex-column align-items-end justify-content-center font-article__right-side">
                                <div class="d-flex align-items-center justify-content-end">
                                    <div class="text-nowrap font-13">
                                        <span class="number-of-styles"></span>
                                        <span>Personal use</span>
                                    </div>
                                    <img data-src="assets/images/bookmark.svg" class="js-lazy font-article__bookmark">
                                </div>
                                <button class="font-article__download-btn">Download</button>
                            </div>
                        </article>
                    </div>
                    <nav class="navigation pagination" role="navigation" aria-label=" ">
                        <div class="nav-links">
                            <a class="prev page-numbers d-none" href="#">
                                <span class="pagination__arrow-container">→</span>
                                Prev
                            </a>
                            <div class="scroll-horizontally">
                                <a class="page-numbers current" href="#">1</a>
                                <a class="page-numbers" href="#">2</a>
                                <a class="page-numbers" href="#">3</a>
                                <span class="page-numbers more">...</span>
                                <a class="page-numbers" href="#">196</a>
                            </div>
                            <a class="next page-numbers" href="#">
                                Next
                                <span class="pagination__arrow-container">→</span>
                            </a>
                        </div>
                    </nav>
                    <div class="flyers__desc closed">
                        <h2 class="flyers__desc__heading">Heading Two</h2>
                        <p>Selection of iPhone mockups in frontal, perspective, isometric and other angles and materials
                            like clay, realistic and simple shapes in PSD, PNG and Sketch format. Whether you’re a
                            student designing an app, or someone who has dreamed of their own project from the ground up
                            — it’s time to get serious about this. We’ve compiled some iPhone mockups for your
                            convenience! It includes minimalistic simplified/clay style as well as realistic and flat
                            design in different angles including front-facing perspective and isometric ones. Lorem
                            ipsum dolor sit amet, consectetur adipisicing elit. Amet architecto aspernatur cum
                            cupiditate deleniti distinctio dolores eligendi expedita fuga fugit impedit iste libero
                            minus necessitatibus nihil odio, optio pariatur perferendis praesentium quasi quibusdam quo
                            rem sequi, suscipit vero vitae voluptas. Ab architecto, cumque deleniti dolores magni, natus
                            obcaecati provident reiciendis, repellat sed sint temporibus vitae voluptas? Dolorum ea est
                            sapiente tenetur unde voluptate. Distinctio fugit labore laborum quidem quisquam ut vitae?
                            Amet asperiores assumenda beatae distinctio dolorum ea enim eum hic iste labore laudantium,
                            nostrum nulla numquam odio perspiciatis temporibus tenetur. Dolores dolorum excepturi
                            facilis fugit quasi ullam unde ut!</p>
                        <button>Show more</button>
                    </div>
                </section>
            </div>
        </main>
        <div class="popup-container">
<div class="popup-loading-container">
         <div class="popup-loading"></div>
     </div>
            <button class="close-btn"></button>
            <button class="next-btn"></button>
            <button class="prev-btn"></button>
            <div class="popup" id="popup">
            </div>
        </div>
    </div>
<?php include 'footer.php'; ?>